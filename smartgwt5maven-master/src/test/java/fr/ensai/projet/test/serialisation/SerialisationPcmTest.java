package fr.ensai.projet.test.serialisation;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.projet.server.metier.Commentaire;
import fr.ensai.projet.server.metier.Feature;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.metier.Product;
import fr.ensai.projet.server.metier.TypeBoolean;
import fr.ensai.projet.server.serialisation.PcmSerializer;

public class SerialisationPcmTest {

	private Pcm pcm;
	private Product product1;
	private Feature feature1;
	private Feature feature2;
	private Commentaire com;
	private PcmSerializer pcmSerializer;
	private final String FOLDER = "pcm_dispos";

	@Before
	public void setUp() throws Exception {
		pcmSerializer = new PcmSerializer();
		pcm = new Pcm("monPcm", "maDescription", "team");
		com = new Commentaire("foo", "team", new Date());
		product1 = new Product("1", "product1", pcm);
		feature1 = new Feature("2", "feature1", pcm);
		feature2 = new Feature("3", "feature2", pcm, new TypeBoolean());
		pcm.addProduct(product1);
		pcm.addFeature(feature1);
		pcm.addFeature(feature2);
		product1.addCommentaire(com);
	}

	@Test
	public void exportImportPcm() {
		try {
			System.err.println(pcmSerializer.exportPcm(pcm));
			// importPcm();
		} catch (UnsupportedEncodingException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void importPcm() {
		try {
			pcmSerializer.exportPcm(pcm);
			Pcm pcmImport = pcmSerializer.importPcm("monPcm");
			String pcmXml = new String(Files.readAllBytes(Paths.get(FOLDER
					+ "/" + "monPcm.xml")));
			assertEquals(pcmXml, serialize(pcmImport));
		} catch (IOException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String serialize(Pcm pcm) throws JAXBException,
			UnsupportedEncodingException {
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		JAXBContext jaxbContext = JAXBContext.newInstance(Pcm.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(pcm, outStream);

		String monPcmInXml = outStream.toString("UTF-8");
		return monPcmInXml;
	}

}
