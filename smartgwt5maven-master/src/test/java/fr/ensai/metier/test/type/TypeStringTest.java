package fr.ensai.metier.test.type;

import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.projet.server.metier.TypeString;

public class TypeStringTest {

	private TypeString ts;

	@Before
	public void setUp() throws Exception {
		ts = new TypeString();
	}

	@Test
	public void verifyConstraintFalse() {
		assertFalse(
				"",
				ts.verifyConstraints("AAAAAAAAAAAAAAAKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
						+ "KKKKKKKKKKKKKKKKKKNNNNNNNBBBBBBBBBBBBBBBBBBBBBBGGGGGG"
						+ "GGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
						+ "CCCCCCCPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
						+ "AAAAAAAAAAAAAAAKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
						+ "KKKKKKKKKKKKKKKKKKNNNNNNNBBBBBBBBBBBBBBBBBBBBBBGGGGGG"
						+ "GGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
						+ "CCCCCCCPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
						+ "AAAAAAAAAAAAAAAKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
						+ "KKKKKKKKKKKKKKKKKKNNNNNNNBBBBBBBBBBBBBBBBBBBBBBGGGGGG"
						+ "GGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
						+ "CCCCCCCPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPQQQQQQQQQQQQQQQQQQQQQQQQQQQ"
						+ "AAAAAAAAAAAAAAAKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK"
						+ "KKKKKKKKKKKKKKKKKKNNNNNNNBBBBBBBBBBBBBBBBBBBBBBGGGGGG"
						+ "GGGGGGGGGGGGCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
						+ "CCCCCCCPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPQQQQQQQQQQQQQQQQQQQQQQQQQQQ"));
	}

}
