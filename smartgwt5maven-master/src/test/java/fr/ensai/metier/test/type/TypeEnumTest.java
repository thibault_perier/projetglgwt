package fr.ensai.metier.test.type;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import fr.ensai.projet.server.metier.TypeEnum;

public class TypeEnumTest {
	
	private TypeEnum te;
	
	@Before
	public void setUp() throws Exception {
		Set<String> values = new HashSet<String>();
		values.add("honda");
		values.add("peugeot");
		values.add("citroen");
		values.add("toyota");
		values.add("bmw");
		values.add("mercedes");
		values.add("Dodge");
		values.add("GMC");
		te = new TypeEnum(values);
	}

	// Test avec le constructeur par défaut

	@Test
	public void testVerifConstraintTrue() {
	assertTrue("Honda verifyTrue", te.verifyConstraints("Honda"));
	assertTrue("gmc verifyTrue", te.verifyConstraints("gmc"));
	}
	
	@Test
	public void testVerifConstraintFalse() {
	assertFalse("Hond verifyFalse", te.verifyConstraints("Hond"));
	assertFalse("GMP verifyFalse", te.verifyConstraints("GMP"));
	}
	
	@Test
	public void update() {
	assertEquals("Honda update","Honda", te.update("Honda", "honda"));
	}


}
