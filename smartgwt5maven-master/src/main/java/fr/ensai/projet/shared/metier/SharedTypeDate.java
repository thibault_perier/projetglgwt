package fr.ensai.projet.shared.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class SharedTypeDate extends SharedType implements IsSerializable{

	/**
	 * formatter : format des dates pour une feature particulière
	 */
	public static List<String> patternsAvailable = patternAvailable();

	//String pattern;

	/**
	 * Constructeur par défaut. Pattern="DD/MM/YYYY"
	 */
	public SharedTypeDate() {
		//pattern = patternsAvailable.get(0);
	}

	/**
	 * Constructeur avec choix du format de la date.
	 * 
	 * @param pattern
	 *            : format de la date.
	 */
	public SharedTypeDate(int nbPattern) {
		if (nbPattern < patternsAvailable.size()) {
		//	pattern = patternsAvailable.get(nbPattern);
		} else {
			//pattern = patternsAvailable.get(0);
		}
	}

	public String create() {
		return "";
	}

	public boolean verifyConstraints(String value) {
		boolean matches=false;
		for (String pattern : patternsAvailable) {
			if (value.matches(pattern)) {
				matches=true;
				break;
			}
		}
		return matches ;
	}

	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	public Object update(String newValue, String oldValue) {
		oldValue = (new Date(newValue)).toString();
		return oldValue;
	}

	/**
	 * 
	 * @return
	 */
	private static List<String> patternAvailable() {
		List<String> patternsAvailable = new ArrayList<String>();
		String defaultPattern1 = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";
		patternsAvailable.add(defaultPattern1);
		//Possibilités d'autres pattern --> mois, jours, format de date, etc...
		//String defaultPattern2 = "((0?[1-9]|1[012]))";
		//patternsAvailable.add(defaultPattern2);
		return patternsAvailable;
	}

}
