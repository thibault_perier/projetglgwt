package fr.ensai.projet.shared.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class SharedPcm implements IsSerializable {

	private int id;

	/**
	 * Attributs indispensables a la construction du pcm
	 */
	private String name = "";
	private String description = "";
	private String author = "";
	private Date dateCreation;
	private Date dateDerniereModif;

	/**
	 * Composants du pcm
	 */

	private Set<SharedProduct> productsShared = new HashSet<SharedProduct>();
	private Set<SharedFeature> featuresShared = new HashSet<SharedFeature>();
	private Set<SharedValuedCell> valuedCellsShared = new HashSet<SharedValuedCell>();
	private Set<SharedFeature> kingfeatures = new HashSet<SharedFeature>();
	public SharedPcm() {
	}

	/*
	 * Accesseurs et mutateurs
	 */
	public int getId() {
		return id;
	}

	public Set<SharedFeature> getKingfeatures() {
		return kingfeatures;
	}

	public void setKingfeatures(Set<SharedFeature> kingfeatures) {
		this.kingfeatures = kingfeatures;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateDerniereModif() {
		return dateDerniereModif;
	}

	public void setDateDerniereModif(Date dateDerniereModif) {
		this.dateDerniereModif = dateDerniereModif;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public Set<SharedValuedCell> getValuedCellsShared() {
		return valuedCellsShared;
	}

	/*
	 * Constructeurs
	 */

	/**
	 * Constructor
	 * 
	 * @param name
	 * @param description
	 * @param author
	 */
	public SharedPcm(String name, String description, String author) {
		super();
		this.name = name;
		this.description = description;
		this.author = author;
		this.dateCreation = new Date();
		this.dateDerniereModif = this.dateCreation;
	}

	public Set<SharedFeature> getFeaturesShared() {
		return featuresShared;
	}

	public void setFeaturesShared(Set<SharedFeature> featuresShared) {
		this.featuresShared = featuresShared;
	}

	/**
	 * Overrides of usual operator on Set
	 */
	public boolean addProduct(SharedProduct product) {
		if (this.productsShared.add(product)) {
			for (SharedFeature feature : featuresShared) {
				SharedValuedCell cell = new SharedValuedCell(product, feature);
				valuedCellsShared.add(cell);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Overrides of usual operator on Set for copy
	 */
	public boolean addProductCopy(SharedProduct product) {

		if (this.productsShared.add(product)) {
			this.dateDerniereModif = new Date();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Remove product from PCM.
	 * @param product
	 * @return
	 */
	public boolean removeProduct(SharedProduct product) {
		if (this.productsShared.remove(product)) {

			List<SharedValuedCell> temp = getValuedCellsProduct(product);
			this.valuedCellsShared.removeAll(temp);

			return true;
		} else {
			return false;
		}
	}

	/**
	 * Add feature in PCM.
	 * @param feature
	 * @return
	 */
	public boolean addFeature(SharedFeature feature) {
		if (feature.isKing()) {
			return kingfeatures.add(feature);
		} else if (!feature.isKing()
				&& this.featuresShared.add((SharedFeature) feature)) {
			for (SharedProduct product : productsShared) {
				SharedValuedCell cell = new SharedValuedCell(product,
						(SharedFeature) feature);
				valuedCellsShared.add(cell);
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Remove feature from PCM.
	 * @param feature
	 * @return
	 */
	public boolean removeFeature(SharedFeature feature) {
		if (this.featuresShared.remove(feature)) {

			List<SharedValuedCell> temp = getValuedCellsFeature(feature);
			this.valuedCellsShared.removeAll(temp);

			return true;
		} else if (this.kingfeatures.remove(feature)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Update last modif.
	 */
	public void updateLastModif() {
		dateDerniereModif = new Date();
	}

	/**
	 * Recupère les valuedcells pour un produit.
	 * @param product
	 * @return
	 */
	public List<SharedValuedCell> getValuedCellsProduct(SharedProduct product) {
		List<SharedValuedCell> iValuedCellsProduct = new ArrayList<SharedValuedCell>();
		for (SharedValuedCell valuedCell : valuedCellsShared) {
			if (valuedCell.getProduct().equals(product)) {
				iValuedCellsProduct.add(valuedCell);
			}
		}
		return iValuedCellsProduct;
	}

	/**
	 * Recupère les valuedcells pour une feature.
	 * @param feature
	 * @return
	 */
	public List<SharedValuedCell> getValuedCellsFeature(SharedFeature feature) {
		List<SharedValuedCell> valuedCellsFeature = new ArrayList<SharedValuedCell>();
		for (SharedValuedCell valuedCell : valuedCellsShared) {
			if (valuedCell.getFeature().equals(feature)) {
				valuedCellsFeature.add(valuedCell);
			}
		}
		return valuedCellsFeature;
	}

	/**
	 * Recupère les valuedcells pour un produit et une feature.
	 * @param feature
	 * @param product
	 * @return
	 */
	public SharedValuedCell getValuedCellsFeature(SharedFeature feature,
			SharedProduct product) {
		SharedValuedCell ValuedCellFeatureProduct = null;
		for (SharedValuedCell icell : valuedCellsShared) {
			if (icell.getProduct().equals(product)
					&& icell.getFeature().equals(feature)) {

				ValuedCellFeatureProduct = icell;
			}
		}
		System.err.println("SharedPcm.getValuedCellsFeature()");
		System.err.println(ValuedCellFeatureProduct);
		return ValuedCellFeatureProduct;
	}

	public Set<SharedProduct> getProducts() {
		return productsShared;
	}

	/**
	 * Methode permettant de trouver un produit grace a son id
	 * 
	 * @param id
	 *            du product recherche
	 * @return product si trouve dans le pcm, null sinon
	 */
	public SharedProduct findProductById(String id) {
		SharedProduct productWithSameId = null;
		for (SharedProduct product : productsShared) {
			if (product.getId().equals(id)) {
				productWithSameId = product;
			}
		}
		return productWithSameId;
	}

	/**
	 * Methode permettantd de trouver une feature grace a son id
	 * 
	 * @param id
	 *            de la feature recherchee
	 * @return feature trouvee ou null si la feature n'a pas ete trouvee
	 */
	public SharedFeature findFeatureById(String id) {
		SharedFeature featureWithSameId = null;
		for (SharedFeature feature : featuresShared) {
			if (feature.getId().equals(id)) {
				featureWithSameId = feature;
			}
		}
		return featureWithSameId;
	}

	/**
	 * Methode permettant d'ajouter une valuedCell au pcm.
	 * 
	 * @param valuedCell
	 */
	public void addValuedCell(SharedValuedCell valuedCell) {
		valuedCell.setPcm(this);
		this.valuedCellsShared.add(valuedCell);
	}

	/**
	 * Methode permettant d'obtenir le nombre de feature
	 * 
	 * @param valuedCell
	 */
	public int getNbFeature() {
		return featuresShared.size();
	}
}
