package fr.ensai.projet.shared.metier;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SharedFeature extends SharedHeader implements IsSerializable{

	/*
	 * Attributs
	 */
	private SharedType typeShared;
	private boolean isKing;
	private Set<SharedFeature> children = new HashSet<SharedFeature>();
	private SharedFeature parent = null;

	
	/*
	 * Accesseurs et mutateurs
	 */
	public SharedType getType() {
		return this.typeShared;
	}

	public SharedType getTypeShared() {
		return typeShared;
	}

	public void setTypeShared(SharedType typeShared) {
		this.typeShared = typeShared;
	}

	public Set<SharedFeature> getChildren() {
		return this.children;
	}

	public void setChildren(Set<SharedFeature> children) {
		this.children.addAll(children);

	}

	public boolean addChild(SharedFeature child) {
		if (child.getParent() == null) {
			child.setParent(this);
			return children.add(child);
		} else {
			return false;
		}
	}

	public boolean removeChild(SharedFeature child) {
		child.removeParent();
		return children.remove(child);
	}

	public boolean isChildOf(SharedFeature header) {
		return this.parent.equals(header);
	}

	public SharedFeature getParent() {
		return this.parent;
	}

	public void setParent(SharedFeature parent) {
		this.parent = parent;
	}

	public void removeParent() {
		this.parent = null;
	}

	public boolean isParentOf(SharedFeature header) {
		if (this.children != null && this.children.contains(header)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isKing() {
		return isKing;
	};

	/*
	 * Constructeur
	 */

	/**
	 * Constructor for a simple feature
	 * 
	 * @param idHeader
	 * @param nameFeature
	 * @param type
	 * @param pcm
	 */
	public SharedFeature(String id,String nameFeature, SharedType type, SharedPcm pcm) {
		super(id,nameFeature, pcm);
		this.typeShared = type;
		this.isKing = false;
	}

	/**
	 * 
	 * Constructor for a feature family
	 * 
	 * @param idHeader
	 * @param nameFeature
	 * @param pcm
	 */
	public SharedFeature(String id,String nameFeature, SharedPcm pcm) {
		super(id,nameFeature, pcm);
		this.isKing = true;
	}
	
	public SharedFeature(String nameFeature, SharedPcm pcm) {
		super(nameFeature, pcm);
		this.isKing = true;
	}
	
	public SharedFeature(String nameFeature, SharedType type, SharedPcm pcm) {
		super(nameFeature, pcm);
		this.typeShared = type;
		this.isKing = false;
	}

	
	public SharedFeature() {
		super();
	}
}
