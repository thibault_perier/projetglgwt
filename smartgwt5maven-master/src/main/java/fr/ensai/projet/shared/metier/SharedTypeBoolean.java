package fr.ensai.projet.shared.metier;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SharedTypeBoolean extends SharedType implements IsSerializable{

	private Set<String> values = new HashSet<String>();

	/**
	 * Constructeurs
	 */
	public SharedTypeBoolean() {
		super();
		values.add("true");
		values.add("false");
		values.add("t");
		values.add("f");
		values.add("1");
		values.add("0");
		values.add("");
	}


	public String create() {
		return "";
	}

	public boolean verifyConstraints(String value) {
		return values.contains(value.toLowerCase());
	}

	public String update(String newValue, String oldValue) {
		oldValue = newValue;
		return oldValue;
	}
}
