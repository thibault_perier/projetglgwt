package fr.ensai.projet.shared.metier;

import java.util.HashSet;
import java.util.Set;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SharedProduct extends SharedHeader implements IsSerializable{

	/*
	 * Attributs
	 */

	private Set<SharedCommentaire> commentairesShared;

	/*
	 * Accesseurs et mutateurs
	 */

	public Set<SharedCommentaire> getCommentairesShared() {
		return commentairesShared;
	}

	/*
	 * Constructeurs
	 */

	/**
	 * Constructor for a simple product
	 * 
	 * @param idProduct
	 * @param name
	 * @param pcmShared
	 * @param isKing
	 */
	public SharedProduct(String name, SharedPcm pcmShared) {
		super(name, pcmShared);
		this.commentairesShared = new HashSet<SharedCommentaire>();
	}
	
	/**
	 * Fonction utile pour la copie entre serveur et shared.
	 * @param commentaire
	 * @return
	 */
	public boolean addCommentaire(SharedCommentaire commentaire) {
		commentaire.setProduct(this);
		return commentairesShared.add(commentaire);
	}
	
	/**
	 * Constructeur  pour un produit pour la copies
	 * 
	 * @param name
	 *            nom du produit
	 * @param pcm
	 *            auquel appartient le produit
	 * @param parent
	 *            famille de produits éventuellement null
	 */
	public SharedProduct(String id,String name, SharedPcm pcm,
			Set<SharedCommentaire> commentaires) {
		super(id,name, pcm);
		this.commentairesShared=commentaires;
		for (SharedCommentaire commentaire : commentairesShared) {
			addCommentaire(commentaire);
		}
	}
	
	public SharedProduct(){
		super();
	}

}
