package fr.ensai.projet.shared.metier;

import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlElement;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class SharedTypeEnum extends SharedType implements IsSerializable{

	@XmlElement(name = "values")
	private Set<String> values;

	/**
	 * Constructeurs
	 */
	public SharedTypeEnum() {
		super();
	}
	public SharedTypeEnum(Set<String> values) {
		super();
		this.values = new HashSet<String>();
		for (String string : values) {
			this.values.add(string.toLowerCase());
		}
	}

	public String create() {
		return "";
	}

	public boolean verifyConstraints(String value) {
		return values.contains(value.toLowerCase());
	}

	public Object update(String value, String oldvalue) {
		oldvalue = value;
		return oldvalue;
	}
	public Set<String> getValues() {
		return values;
	}
	public void setValues(Set<String> values) {
		this.values = values;
	}
	
	
}
