package fr.ensai.projet.shared.metier;

import com.google.gwt.user.client.rpc.IsSerializable;

public abstract class SharedType implements IsSerializable{

	/*
	 * Attribut
	 */

	protected String description;

	/*
	 * Accesseurs et mutateurs
	 */

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * Autres methodes abstraites
	 */
	public abstract String create();

	public abstract boolean verifyConstraints(String value);

	public abstract Object update(String newValue, String oldValue);

}
