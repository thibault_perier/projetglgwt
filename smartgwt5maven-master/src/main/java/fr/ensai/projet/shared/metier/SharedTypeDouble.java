package fr.ensai.projet.shared.metier;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

public class SharedTypeDouble extends SharedType implements IsSerializable{

	private Integer rangeMin=Integer.MIN_VALUE;
	private Integer rangeMax=Integer.MAX_VALUE;
	private int nbDecimaleMax=2;

	public SharedTypeDouble() {
	}

	public SharedTypeDouble(int rangeMin, int rangeMax, int nbDecimaleMax) {
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
		this.nbDecimaleMax = nbDecimaleMax;
	}

	public String create() {
		return "";
	}

	public boolean verifyConstraints(String value) {
		Logger logger = Logger.getLogger("Import du PCM success");
	
		try {
			value=value.replace(",", ".");
			Double test = Double.parseDouble(value);
			logger.log(Level.INFO, "ok");
			return test >= rangeMin && test <= rangeMax;

		} catch (NumberFormatException e) {		
			logger.log(Level.INFO, "no");
			return false;
		}

	}

	public String update(String value) {
		Double nb = Double.parseDouble(value);
		String nbFormatString = getFormatted(nb, this.nbDecimaleMax);
		Double nbFormat = Double.parseDouble(nbFormatString);
		return nbFormat.toString();
	}

	/**
	 * Formattage d'un double par rapport à un nombre de décimalesMax.
	 * @param value
	 * @param nbDecimalMax
	 * @return
	 */
	private static String getFormatted(Double value, int nbDecimalMax) {
	    StringBuilder numberPattern = new StringBuilder(
	            (nbDecimalMax <= 0) ? "" : ".");
	    for (int i = 0; i < nbDecimalMax; i++) {
	        numberPattern.append('#');
	    }
	    return NumberFormat.getFormat("00.00").format(value).replace(",", ".");
	}

	@Override
	public Object update(String newValue, String oldValue) {
		// TODO Auto-generated method stub
		return null;
	}
}
