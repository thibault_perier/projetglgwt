package fr.ensai.projet.shared.metier;

import com.google.gwt.user.client.rpc.IsSerializable;


public abstract class SharedHeader implements IsSerializable{

	private String id;
	protected static long longId = 0;
	protected String name;
	protected SharedPcm pcmShared;



	/*
	 * Accesseurs et mutateurs + surcharge des operateurs usuels de conteneurs
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String newName) {
		this.name = newName;
	}

	public SharedPcm getPcmShared() {
		return pcmShared;
	}


	/**
	 * Constructeur
	 * 
	 * @param name
	 * @param pcm
	 */
	public SharedHeader(String name, SharedPcm pcm) {
		this.id = "" + (longId++);
		this.name = name;
		this.pcmShared = pcm;
	}
	
	public SharedHeader(String id, String name, SharedPcm pcm) {
		this.id = id;
		this.name = name;
		this.pcmShared = pcm;
		longId++;
	}

	public SharedHeader() {
	
	}

}
