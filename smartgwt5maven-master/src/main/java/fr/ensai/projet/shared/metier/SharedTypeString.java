package fr.ensai.projet.shared.metier;

import com.google.gwt.user.client.rpc.IsSerializable;


public class SharedTypeString extends SharedType implements IsSerializable{

private final int sizeMax = 100;

	public SharedTypeString() {
	}

	public String create() {
		return new String();
	}

	public boolean verifyConstraints(String value) {
		return value.length() <= this.sizeMax;
	}

	public String update(String value, String oldValue) {
		oldValue = value;
		return oldValue;
	}

}
