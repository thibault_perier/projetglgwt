package fr.ensai.projet.shared.metier;

import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.rpc.IsSerializable;

public class SharedPatternBis implements IsSerializable{
	public long id;
	private RegExp pattern;
	private String description;

	public SharedPatternBis() {
		super();
	}

	public SharedPatternBis(RegExp pattern, String description) {
		super();
		this.pattern = pattern;
		this.description = description;
	}

	public RegExp getPattern() {
		return pattern;
	}

	public void setPattern(RegExp pattern) {
		this.pattern = pattern;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
