package fr.ensai.projet.shared.metier;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SharedValuedCell implements IsSerializable{

	/*
	 * Attributs
	 */
	private SharedPcm pcm;
	private SharedProduct product;
	private SharedFeature feature;
	private String value;
	private SharedType type;

	/*
	 * Constructeurs
	 */

	/**
	 * Constructeur
	 * 
	 * @param product
	 * @param feature
	 */
	public SharedValuedCell(SharedProduct product, SharedFeature feature) {
		// TODO Auto-generated method stub
		super();
		this.feature = feature;
		this.product = product;
		if (feature.getType()!=null) {
			this.type = feature.getType();
			this.value = feature.getType().create();
		}
	}
	
	public SharedValuedCell() {
		super();
	
	}


	/**
	 * Accesseurs et mutateurs
	 */
	public SharedPcm getPcm() {
		return pcm;
	}

	public SharedProduct getProduct() {
		return product;
	}

	public SharedFeature getFeature() {
		return feature;
	}

	public String getValue() {
		return value;
	}

	/**
	 * Change the value of the cell
	 * 
	 * @param value
	 *            to update, the format of value will be verified
	 * @return true if the value has been updated correctly
	 */
	public boolean updateValue(String value) {
		if (checkType(value)) {
			this.value = value;			
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Private method to know if the new value can replace the current
	 * 
	 * @param value
	 *            new value for the cell
	 * @return true if the new value has the right format
	 */
	private boolean checkType(String value) {
		boolean verifyC=type.verifyConstraints(value);
		return verifyC;
	}

	public SharedType getType() {
		return type;
	}

	public void setType(SharedType type) {
		this.type = type;
	}

	public void setPcm(SharedPcm pcm) {
		this.pcm = pcm;
	}

	public void setProduct(SharedProduct product) {
		this.product = product;
	}

	public void setFeature(SharedFeature feature) {
		this.feature = feature;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	

}
