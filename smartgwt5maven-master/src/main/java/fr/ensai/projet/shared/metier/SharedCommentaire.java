package fr.ensai.projet.shared.metier;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class SharedCommentaire implements IsSerializable {

	/**
	 * Attributs
	 */
	private String texte;
	private String auteur;
	private Date date;
	private SharedProduct product;

	/**
	 * Constructeurs utilisant tous les attributs
	 * 
	 * @param texte
	 * @param auteur
	 * @param date
	 * @param product
	 */
	public SharedCommentaire(String texte, String auteur, Date date,
			SharedProduct product) {
		super();
		this.texte = texte;
		this.auteur = auteur;
		this.date = date;
		this.product = product;
	}

	/**
	 * Constructeurs utilisant tous les attributs sauf produit
	 * 
	 * @param texte
	 * @param auteur
	 * @param date
	 * @param product
	 */
	public SharedCommentaire(String texte, String auteur, Date date) {
		super();
		this.texte = texte;
		this.auteur = auteur;
		this.date = date;
	}

	public SharedCommentaire(String texte, String auteur, SharedProduct product) {
		super();
		this.texte = texte;
		this.auteur = auteur;
		this.product = product;
		this.date = new Date();
	}

	public SharedCommentaire(String texte, SharedProduct product) {
		super();
		this.texte = texte;
		this.auteur = "";
		this.date = new Date();
		this.product = product;
	}

	public SharedCommentaire() {
		super();
	}

	/**
	 * Accesseurs et mutateurs
	 * 
	 * @return
	 */
	public String getTexte() {
		return texte;
	}

	public String getAuteur() {
		return auteur;
	}

	public Date getDate() {
		return date;
	}

	public SharedProduct getProduct() {
		return product;
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setProduct(SharedProduct product) {
		this.product = product;
	}

}
