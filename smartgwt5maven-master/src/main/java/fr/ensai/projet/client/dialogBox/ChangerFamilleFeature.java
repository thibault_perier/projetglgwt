package fr.ensai.projet.client.dialogBox;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ValueListBox;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.client.column.IColumn;
import fr.ensai.projet.shared.metier.SharedFeature;

/**
 * Methode pour changer la famille d'une feature
 */
public class ChangerFamilleFeature {

	/*
	 * Attributs
	 */
	/*
	 * Conteneur de la fenetre Pop up qui s'affiche lorsque l'on souhaite
	 * changer la famille d'une feature.
	 */
	private final DialogBox dialogBox;
	/*
	 * Contenu de la dialogBox.
	 */
	private final HorizontalPanel dialogContents;
	private final HorizontalPanel buttonsPanel = new HorizontalPanel();
	private final ValueListBox<SharedFeature> lisBoxAvailableFeature = new ValueListBox<SharedFeature>(
			new Renderer<SharedFeature>() {

				@Override
				public String render(SharedFeature object) {
					String render = "";
					if (object != null) {
						render = object.getName().toLowerCase();

					}
					return render;
				}

				@Override
				public void render(SharedFeature object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);

				}
			});
	private final ValueListBox<SharedFeature> lisBoxAvailableKingFeature = new ValueListBox<SharedFeature>(
			new Renderer<SharedFeature>() {

				@Override
				public String render(SharedFeature object) {
					String render = "";
					if (object != null) {

						render = "[" + object.getName().toUpperCase() + "]";

					}
					return render;
				}

				@Override
				public void render(SharedFeature object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);

				}
			});

	/*
	 * Accesseur
	 */
	public PopupPanel getDialogBox() {
		return dialogBox;
	}

	/*
	 * Constructeur
	 */
	public ChangerFamilleFeature() {
		dialogBox = new DialogBox();
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.ensureDebugId("RemoveFeatureDialogBox");
		dialogBox.setTitle("Remove Feature");
		dialogContents = new HorizontalPanel();
		dialogContents.setSpacing(10);
		dialogBox.setWidget(dialogContents);
		dialogContents.add(lisBoxAvailableFeature);
		dialogContents.add(lisBoxAvailableKingFeature);
		getAllFeatures();
		addButtons();
	}

	/**
	 * Options disponible dans la listBox du choix des features a selectionner.
	 */
	private void getAllFeatures() {
		Set<SharedFeature> features = IHM.PCM.getFeaturesShared();
		Set<SharedFeature> kingFeatures = getFamille();
		lisBoxAvailableFeature.setAcceptableValues(features);
		lisBoxAvailableKingFeature.setAcceptableValues(kingFeatures);
	}

	/**
	 * Options disponible dans la listBox du choix de la Famille.
	 */
	private Set<SharedFeature> getFamille() {
		Set<SharedFeature> featuresKing = new HashSet<SharedFeature>();
		for (SharedFeature feature : IHM.PCM.getKingfeatures()) {
			featuresKing.add((SharedFeature) feature);
		}
		return featuresKing;

	}

	/**
	 * Creation et ajout du button Cancel (button d'annulation)
	 * 
	 * @param buttonsPanel
	 */
	private void addCancelButton() {
		Button button = new Button("Cancel");
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});

		buttonsPanel.add(button);
	}

	/**
	 * Ajout des boutons cancel et create dans la boite de dialogue.
	 */
	private void addButtons() {
		addCancelButton();
		addModifyButton();
		dialogContents.add(buttonsPanel);
	}

	/**
	 * Creation et ajout du button modifier (button de validation)
	 * 
	 * @param buttonsPanel
	 */
	private void addModifyButton() {
		Button button = new Button("Modifier");
		button.addClickHandler(new ClickHandler() {

			@SuppressWarnings("unchecked")
			@Override
			public void onClick(ClickEvent event) {

				SharedFeature featureMofif = lisBoxAvailableFeature.getValue();
				SharedFeature featureMofifKing = lisBoxAvailableKingFeature
						.getValue();
				if (featureMofif != null) {

					IColumn columnModif = null;
					for (IColumn myColumn : IHM.myColumns) {
						if (myColumn.getSharedFeature().equals(featureMofif)) {
							columnModif = myColumn;
							break;
						}
					}

					featureMofif.setParent(null);
					if (featureMofifKing != null) {

						featureMofifKing.addChild(featureMofif);
						IHM.table.removeColumn(columnModif.getNameColumn());
						IHM.table.insertColumn(
								CreerFeature.positionColumn(columnModif
										.getSharedFeature()), columnModif
										.getNameColumn(), "["
										+ featureMofif.getParent().getName()
												.toUpperCase() + "] "
										+ featureMofif.getName().toLowerCase());

					} else {
						IHM.table.removeColumn(columnModif.getNameColumn());
						IHM.table.insertColumn(
								CreerFeature.positionColumn(columnModif
										.getSharedFeature()), columnModif
										.getNameColumn(), featureMofif
										.getName().toLowerCase());
					}
					dialogBox.hide();
				}
				IHM.updateVariables();
			}
		});

		buttonsPanel.add(button);
	}
}
