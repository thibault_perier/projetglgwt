package fr.ensai.projet.client.service;

import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import fr.ensai.projet.shared.metier.SharedPcm;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {

	/**
	 * Methode for exportation of the current PCM
	 * 
	 * @param ts
	 * @return
	 * @throws IllegalArgumentException
	 */
	String greetServer(SharedPcm ts) throws IllegalArgumentException;

	/**
	 * Method for importation a PCM
	 * 
	 * @return
	 * @throws IllegalArgumentException
	 */
	SharedPcm greetServer2() throws IllegalArgumentException;

	/**
	 * Methode permettant d'importer un PCM avec son nom
	 * 
	 * @param name
	 *            Objet de classe String
	 * @return SharedPcm
	 * @throws IllegalArgumentException
	 */
	public SharedPcm importWithName(String name)
			throws IllegalArgumentException;

	/**
	 * Methode permettant d'enregistrer un PCM au format xml sur le serveur
	 * 
	 * @param sharedPcm
	 * @return String, message de confirmation de la sauvegarde avec le nom du
	 *         fichier
	 * @throws IllegalArgumentException
	 */
	String sauvegarder(SharedPcm sharedPcm) throws IllegalArgumentException;

	/**
	 * Methode permettant d'obtenir la liste des noms des pcm disponibles sur le
	 * serveur
	 * 
	 * @return String[]
	 * @throws IllegalArgumentException
	 */
	Set<String> listePcmPourChargement();
}
