package fr.ensai.projet.client.column;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.shared.metier.SharedProduct;

/**
 * La colonne Name est toujours presente (elle ne correspond pas a une feature).
 * 
 *
 */
public class ColumnName {

	/*
	 * Attributs
	 */
	// Add a text input column to edit the name.
	private final EditTextCell nameCell = new EditTextCell();
	private Column<SharedProduct, String> nameColumn = new Column<SharedProduct, String>(
			nameCell) {
		@Override
		public String getValue(SharedProduct sharedProduct) {
			return sharedProduct.getName();
		}
	};

	/*
	 * Accesseur
	 */
	public Column<SharedProduct, String> getNameColumn() {
		return nameColumn;
	}

	public ColumnName() {
		super();
		// Add a field updater to be notified when the user enters a new name.
		this.nameColumn
				.setFieldUpdater(new FieldUpdater<SharedProduct, String>() {
					@Override
					public void update(int index, SharedProduct object,
							String value) {
						// Inform the user of the change.
						String oldValue = object.getName();

						// Push the changes into the Contact. At this point, you
						// could
						// send an
						// asynchronous request to the server to update the
						// database.
						if (!((String) value).equals("")) {
							if (!oldValue.equals(value)) {
								// Window.alert("You changed the name of " +
								// oldValue
								// + " to " + value);
							}
						} else {
							Window.alert("La valeur " + value
									+ " n'est pas compatible : ");
							nameCell.clearViewData(IHM.KEY_PROVIDER
									.getKey(object));
						}
						// Redraw the table with the new data.
						// Example.dataProvider.refresh();
						IHM.table.redrawRow(index);
					}
				});
	}
}
