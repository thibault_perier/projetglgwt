package fr.ensai.projet.client;

import com.google.gwt.user.cellview.client.CellTable;

public interface TableRes extends CellTable.Resources {
    @Source( { CellTable.Style.DEFAULT_CSS, "cellTable.css" } )
    TableStyle cellTableStyle();

    interface TableStyle extends CellTable.Style {
    }
}
