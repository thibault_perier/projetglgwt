package fr.ensai.projet.client;

import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockPanel;

import fr.ensai.projet.client.dialogBox.ChangerFamilleFeature;
import fr.ensai.projet.client.dialogBox.ChargerPcm;
import fr.ensai.projet.client.dialogBox.CreerFeature;
import fr.ensai.projet.client.dialogBox.SupprimerFeature;
import fr.ensai.projet.client.dialogBox.RenommerFeature;
import fr.ensai.projet.client.dialogBox.ImporterPcmExterne;
import fr.ensai.projet.client.service.GreetingService;
import fr.ensai.projet.client.service.GreetingServiceAsync;
import fr.ensai.projet.shared.metier.SharedProduct;

/**
 * Classe contenant toutes les commandes utilisee dans l'interface
 *
 */
public class Commande {

	/*
	 * Liste des commandes
	 */
	/**
	 * Methode permettant de supprimer un produit du PCM
	 */
	private Command ajouterProduct = new Command() {
		public void execute() {
			SharedProduct product = new SharedProduct("", IHM.PCM);
			IHM.PCM.addProduct(product);
			List<SharedProduct> list = IHM.dataProvider.getList();
			list.add(product);
			IHM.dataProvider.refresh();
			IHM.updateVariables();
		}
	};

	/**
	 * Methode permettant de supprimer un produit
	 */
	private Command supprimerProduct = new Command() {
		public void execute() {
			if (IHM.comment != null && IHM.comment.textArea.isVisible()) {
				IHM.comment.hideComment();
				IHM.modifierTexteCommentaires();
			}

			int selectedRow = IHM.table.getKeyboardSelectedRow();
			if (selectedRow >= 0) {
				SharedProduct productRemove = IHM.dataProvider.getList().get(
						selectedRow);
				IHM.PCM.removeProduct(productRemove);
				IHM.dataProvider.getList().remove(productRemove);
				IHM.dataProvider.refresh();
				IHM.updateVariables();
			}
		}
	};

	/**
	 * Methode permettant d'afficher les commentaires sur un produit. Ces
	 * derniers sont initialements masques
	 */
	private Command lireCommentaire = new Command() {
		public void execute() {
			int selectedRow = IHM.table.getKeyboardSelectedRow();
			if (selectedRow >= 0) {
				SharedProduct productComment = IHM.dataProvider.getList().get(
						selectedRow);
				if (IHM.comment == null
						|| (IHM.comment != null && !IHM.comment.textArea
								.isVisible())) {
					IHM.comment = new Commentaires(productComment);
					IHM.dock.add(IHM.comment, DockPanel.SOUTH);
					IHM.comment.showComment(productComment);
					IHM.modifierTexteCommentaires();
				} else {
					IHM.comment.hideComment();
					IHM.modifierTexteCommentaires();
				}
			}
		}
	};

	/**
	 * Methode permettant d'ajouter une feature
	 */
	private Command ajouterFeature = new Command() {
		public void execute() {
			CreerFeature createFeature = new CreerFeature();
			createFeature.getDialogBox().center();
			createFeature.getDialogBox().show();
			IHM.updateVariables();
		}
	};

	/**
	 * Commande permettant de suupprime rune feature du PCM
	 */
	private Command supprimerFeature = new Command() {
		public void execute() {
			SupprimerFeature removeFeature = new SupprimerFeature();
			removeFeature.getDialogBox().center();
			removeFeature.getDialogBox().show();
			IHM.updateVariables();
		}
	};

	/**
	 * Commande permettant de changer la famille d'une feature
	 */
	private Command changerFamille = new Command() {
		public void execute() {
			ChangerFamilleFeature changeFamille = new ChangerFamilleFeature();
			changeFamille.getDialogBox().center();
			changeFamille.getDialogBox().show();
		}
	};

	/**
	 * Commande permettant de changer la famille d'une feature
	 */
	private Command modifierNom = new Command() {
		public void execute() {
			RenommerFeature modifierNomFeature = new RenommerFeature();
			modifierNomFeature.getDialogBox().center();
			modifierNomFeature.getDialogBox().show();
		}
	};

	/**
	 * Commande permettant d'importer un PCM choisi a partir du poste client
	 */
	private Command importer = new Command() {
		public void execute() {
			ImporterPcmExterne modifierNomFeature = new ImporterPcmExterne();
			modifierNomFeature.getDialogBox().center();
			modifierNomFeature.getDialogBox().show();
		}
	};

	/**
	 * Methode permettant d'exporter un PCM (au format xml)
	 */
	private Command exporter = new Command() {
		public void execute() {
			GreetingServiceAsync greetingSvc = GWT
					.create(GreetingService.class);
			greetingSvc.greetServer(IHM.PCM, new AsyncCallback<String>() {
				Logger logger = Logger.getLogger("Export du PCM");

				@Override
				public void onSuccess(String arg0) {
					String url = "http://" + Window.Location.getHost()
							+ "/projetgl/download?fileName=" + arg0;
					logger.log(Level.INFO, url);
					Window.Location.assign(url);
				}

				@Override
				public void onFailure(Throwable arg0) {
					logger.log(Level.SEVERE, "failure");
				}
			});
		}
	};

	/**
	 * Methode permettant de sauvegarder le PCM (au format xml)
	 */
	private Command sauvegarder = new Command() {
		public void execute() {
			GreetingServiceAsync greetingSvc = GWT
					.create(GreetingService.class);
			greetingSvc.sauvegarder(IHM.PCM, new AsyncCallback<String>() {
				Logger logger = Logger.getLogger("Sauvegarde du PCM");

				@Override
				public void onFailure(Throwable caught) {
					logger.log(Level.SEVERE, "failure");
				}

				@Override
				public void onSuccess(String result) {
					Window.alert(result);
				}

			});
		}
	};

	/**
	 * Methode permettant charger un PCM
	 */
	private Command charger = new Command() {
		public void execute() {
			GreetingServiceAsync greetingSvc = GWT
					.create(GreetingService.class);
			greetingSvc
					.listePcmPourChargement(new AsyncCallback<Set<String>>() {
						Logger logger = Logger.getLogger("Chargement d'un PCM");

						@Override
						public void onFailure(Throwable caught) {
							logger.log(Level.SEVERE, "failure");
						}

						@Override
						public void onSuccess(Set<String> result) {
							ChargerPcm chargerPcm = new ChargerPcm(result);
							chargerPcm.getDialogBox().center();
							chargerPcm.getDialogBox().show();
						}

					});
		}
	};

	/*
	 * Accesseurs et mutateurs
	 */

	public Command getAjouterProduct() {
		return ajouterProduct;
	}

	public Command getSupprimerProduct() {
		return supprimerProduct;
	}

	public Command getLireCommentaire() {
		return lireCommentaire;
	}

	public Command getAjouterFeature() {
		return ajouterFeature;
	}

	public Command getSupprimerFeature() {
		return supprimerFeature;
	}

	public Command getChangerFamille() {
		return changerFamille;
	}

	public Command getModifierNom() {
		return modifierNom;
	}

	public Command getImporter() {
		return importer;
	}

	public Command getExporter() {
		return exporter;
	}

	public Command getSauvegarder() {
		return sauvegarder;
	}

	public Command getCharger() {
		return charger;
	}

	/*
	 * Constructeurs
	 */
	public Commande() {
		super();
	}

}
