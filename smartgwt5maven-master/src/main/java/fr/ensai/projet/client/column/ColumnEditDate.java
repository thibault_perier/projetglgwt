package fr.ensai.projet.client.column;

import java.util.Date;

import com.google.gwt.cell.client.DatePickerCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedProduct;

/**
 * Classe permettant de creer une feature de type Date
 *
 */
public class ColumnEditDate implements IColumn {

	/*
	 * Attributs
	 */
	private SharedFeature sharedFeature;

	// Creation de la colonne et implementation de la methode getValue qui
	// recupere la valeur dans le PCM
	// Add a text input column to edit the name.
	private final DatePickerCell nameCell = new DatePickerCell();
	private Column<SharedProduct, Date> nameColumn = new Column<SharedProduct, Date>(
			nameCell) {
		@Override
		public Date getValue(SharedProduct sharedProduct) {
			@SuppressWarnings("deprecation")
			Date value = new Date(IHM.PCM.getValuedCellsFeature(sharedFeature,
					sharedProduct).getValue());
			return value;
		}
	};

	/*
	 * Accesseurs et mutateurs pour implementer l'interface
	 */
	public Column<SharedProduct, Date> getNameColumn() {
		return nameColumn;
	}

	public SharedFeature getSharedFeature() {
		return sharedFeature;
	}

	/*
	 * Constructeur
	 */

	/**
	 * Constructeur a partir de la sharedFeature correspondant à la colonne
	 * 
	 * @param sharedFeature
	 */
	public ColumnEditDate(SharedFeature sharedFeature) {
		super();
		this.sharedFeature = sharedFeature;
		// Add a field updater to be notified when the user enters a new name.
		this.nameColumn
				.setFieldUpdater(new FieldUpdater<SharedProduct, Date>() {

					@Override
					public void update(int index, SharedProduct object,
							Date value) {
						// Inform the user of the change.
						Object oldValue = IHM.PCM.getValuedCellsFeature(
								getSharedFeature(), object).getValue();

						// Push the changes into the Contact. At this point, you
						// could
						// send an
						// asynchronous request to the server to update the
						// database.
						if (IHM.PCM.getValuedCellsFeature(getSharedFeature(),
								object).updateValue(value.toString())) {
							if (!oldValue.equals(value)) {
								// Window.alert("You changed the name of "
								// + oldValue + " to " + value);
							}
						} else {
							Window.alert("La valeur " + value
									+ " n'est pas compatible : ");
						}
						// Redraw the table with the new data.
						// Example.dataProvider.refresh();
						IHM.table.redrawRow(index);

					}
				});
	}

}
