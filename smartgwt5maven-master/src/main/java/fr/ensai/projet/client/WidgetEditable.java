package fr.ensai.projet.client;

import com.google.gwt.event.dom.client.BlurEvent;
import com.google.gwt.event.dom.client.BlurHandler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Classe permettant d'obtenir des widgets editables avec Label
 * 
 */
public class WidgetEditable extends Composite implements ChangeHandler,
		FocusHandler, BlurHandler {

	/*
	 * Attributs
	 */
	private TextBox textbox = new TextBox();
	private TextArea textArea = new TextArea();
	private VerticalPanel widget = new VerticalPanel();
	private Label label;
	private int type;

	/*
	 * Constructeur
	 */

	/**
	 * Constructeur permettant de creer un widget avec 3 labels differents
	 * 
	 * @param type
	 *            int : 1 pour le NOM du PCM, 2 pour l'auteur du PCM et 3 pour
	 *            les commentaires du PCM
	 */
	public WidgetEditable(int type) {
		this.type = type;

		initWidget(widget);

		switch (type) {
		case 1:
			label = new Label("NOM :");
			widget.add(label);
			initTextBox();
			break;
		case 2:
			label = new Label("AUTEUR :");
			widget.add(label);
			initTextBox();
			break;
		case 3:
			label = new Label("DESCRIPTION :");
			widget.add(label);
			initTextArea();
			break;
		}

		new Timer() {
			public void run() {
				textbox.setFocus(true);
			}
		}.schedule(1);
	}

	/**
	 * Methode permettant d'initialiser les en-tete (label+textBox)
	 * 
	 * @param hint
	 *            String, label de la textBox
	 */
	private void initTextBox() {
		widget.add(textbox);
		widget.setCellWidth(textbox, "100%");
		textbox.setWidth("100%");
		textbox.addChangeHandler(this);
		textbox.addFocusHandler(this);
		textbox.addBlurHandler(this);
	}

	/**
	 * Methode permmettant d'initialise rla sone de commentaire
	 * 
	 * @param hint
	 *            String : label de la zone de commentaires
	 */
	private void initTextArea() {
		widget.add(textArea);
		widget.setCellWidth(textArea, "100%");
		textArea.setWidth("100%");
		textArea.addChangeHandler(this);
		textArea.addFocusHandler(this);
		textArea.addBlurHandler(this);
	}

	/*
	 * Listeners
	 */
	@Override
	public void onBlur(BlurEvent event) {
		IHM.modifierEnTete();
	}

	@Override
	public void onFocus(FocusEvent event) {
		IHM.modifierEnTete();
	}

	@Override
	public void onChange(ChangeEvent event) {
		switch (type) {
		case 1:
			IHM.PCM.setName(textbox.getText());
			break;
		case 2:
			IHM.PCM.setAuthor(textbox.getText());
			break;
		case 3:
			IHM.PCM.setDescription(textArea.getText().toLowerCase());
			break;
		}
		IHM.modifierEnTete();
	}

	/**
	 * Methode permettant d'initialiser contenu des texbox lors de l'import de
	 * nouveaux PCM
	 * 
	 * @param texte
	 *            String
	 * @param type
	 *            int : entre 1 et 3 en fonction de la zone visee
	 */
	public void setTexte(String texte, int type) {
		if (type < 3) {
			textbox.setText(texte);
		} else {
			textArea.setText(texte);
		}
	}

}