package fr.ensai.projet.client.column;

import com.google.gwt.user.cellview.client.Column;

import fr.ensai.projet.shared.metier.SharedFeature;

/**
 * Interface pour les colonne du PCM
 */
public interface IColumn {

	/**
	 * Methode permettant d'obtenir la feature associee a la colonne
	 * 
	 * @return SharedFeature
	 */
	public SharedFeature getSharedFeature();

	/**
	 * Methode pour obtenir la colonne
	 * 
	 * @return Column
	 */
	@SuppressWarnings("rawtypes")
	public Column getNameColumn();
}
