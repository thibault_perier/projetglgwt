package fr.ensai.projet.client.service;

import java.util.Set;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

import fr.ensai.projet.shared.metier.SharedPcm;

public interface GreetingServiceAsync {

	/**
	 * GWT-RPC service asynchronous (client-side) interface
	 * 
	 * @see fr.ensai.projet.client.GreetingService
	 */
	void greetServer(SharedPcm ts, AsyncCallback<java.lang.String> callback);

	/**
	 * Utility class to get the RPC Async interface from client-side code
	 */
	public static final class Util {
		private static GreetingServiceAsync instance;

		public static final GreetingServiceAsync getInstance() {
			if (instance == null) {
				instance = (GreetingServiceAsync) GWT
						.create(GreetingService.class);
			}
			return instance;
		}

		private Util() {
			// Utility class should not be instanciated
		}
	}

	/**
	 * GWT-RPC service asynchronous (client-side) interface
	 * 
	 * @see fr.ensai.projet.client.GreetingService
	 */
	void greetServer2(AsyncCallback<SharedPcm> asyncCallback);

	/**
	 * GWT-RPC service asynchronous (client-side) interface
	 * 
	 * @see fr.ensai.projet.client.GreetingService
	 */
	void importWithName(String name, AsyncCallback<SharedPcm> asyncCallback);

	/**
	 * GWT-RPC service asynchronous (client-side) interface
	 * 
	 * @see fr.ensai.projet.client.GreetingService
	 */
	void sauvegarder(SharedPcm sharedPcm, AsyncCallback<String> asyncCallback);
	
	/**
	 * GWT-RPC service asynchronous (client-side) interface
	 * 
	 * @see fr.ensai.projet.client.GreetingService
	 */
	void listePcmPourChargement(AsyncCallback<Set<String>> asyncCallback);
}
