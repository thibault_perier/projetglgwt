package fr.ensai.projet.client.column;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedProduct;
import fr.ensai.projet.shared.metier.SharedTypeEnum;

/**
 * Classe pour une colonne associee a une feature de type Enum
 */
public class ColumnSelectionEnum implements IColumn {

	/*
	 * Attributs
	 */
	// Add a text input column to edit the name.
	private final SelectionCell nameCell;
	private final SharedFeature sharedFeature;
	private Column<SharedProduct, String> nameColumn;

	/*
	 * Accesseurs et mutateurs pour implementer l'interface
	 */
	public Column<SharedProduct, String> getNameColumn() {
		return nameColumn;
	}

	public SharedFeature getSharedFeature() {
		return sharedFeature;
	}

	/*
	 * Constructeurs
	 */
	public ColumnSelectionEnum(SharedFeature sharedFeature) {
		super();
		this.sharedFeature = sharedFeature;

		List<String> choices = new ArrayList<String>(
				((SharedTypeEnum) this.sharedFeature.getType()).getValues());
		nameCell = new SelectionCell(choices);
		this.nameColumn = new Column<SharedProduct, String>(nameCell) {
			@Override
			public String getValue(SharedProduct object) {
				SharedProduct product = object;
				Object value = IHM.PCM.getValuedCellsFeature(
						getSharedFeature(), product).getValue();
				return value.toString();
			}
		};
		
		this.nameColumn.setFieldUpdater(new FieldUpdater<SharedProduct, String>() {
			
			@Override
			public void update(int index, SharedProduct object, String value) {
				// Inform the user of the change.
				Object oldValue = IHM.PCM.getValuedCellsFeature(
						getSharedFeature(), object).getValue();

				// Push the changes into the Contact. At this point, you
				// could
				// send an
				// asynchronous request to the server to update the
				// database.
				if (IHM.PCM.getValuedCellsFeature(
						getSharedFeature(), object).updateValue(value)) {
					if (!oldValue.equals(value)) {
						// Window.alert("You changed the name of "
						// + oldValue + " to " + value);
					}
				} else {
					Window.alert("La valeur " + value
							+ " n'est pas compatible : ");
					nameCell.clearViewData(IHM.KEY_PROVIDER
							.getKey(object));
				}
				// Redraw the table with the new data.
				// Example.dataProvider.refresh();
				IHM.table.redrawRow(index);
				
			}
		});
	}
}
