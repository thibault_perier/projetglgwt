package fr.ensai.projet.client.column;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedProduct;

/**
 * Classe pour une colonne associee a une feature de type String
 */
public class ColumnEditString implements IColumn {

	/*
	 * Attributs
	 */
	private SharedFeature sharedFeature;

	// Creation de la colonne et implementation de la methode getValue qui
	// recupere la valeur dans le PCM
	// Add a text input column to edit the name.
	private final EditTextCell nameCell = new EditTextCell();
	private Column<SharedProduct, String> nameColumn = new Column<SharedProduct, String>(
			nameCell) {
		@Override
		public String getValue(SharedProduct sharedProduct) {
			String value = IHM.PCM.getValuedCellsFeature(sharedFeature,
					sharedProduct).getValue();
			return value.toString();
		}
	};

	/*
	 * Accesseurs et mutateurs pour implementer l'interface
	 */
	public Column<SharedProduct, String> getNameColumn() {
		return nameColumn;
	}

	public SharedFeature getSharedFeature() {
		return sharedFeature;
	}

	/*
	 * Constructeur
	 */

	/**
	 * Constructeur a partir de la sharedFeature correspondant à la colonne
	 * 
	 * @param sharedFeature
	 */
	public ColumnEditString(SharedFeature sharedFeature) {
		super();
		this.sharedFeature = sharedFeature;
		// Add a field updater to be notified when the user enters a new name.
		this.nameColumn
				.setFieldUpdater(new FieldUpdater<SharedProduct, String>() {
					@Override
					public void update(int index, SharedProduct object,
							String value) {
						// Inform the user of the change.
						Object oldValue = IHM.PCM.getValuedCellsFeature(
								getSharedFeature(), object).getValue();

						// Push the changes into the Contact. At this point, you
						// could
						// send an
						// asynchronous request to the server to update the
						// database.
						if (IHM.PCM.getValuedCellsFeature(getSharedFeature(),
								object).updateValue(value)) {
							if (!oldValue.equals(value)) {
								// Window.alert("You changed the name of "
								// + oldValue + " to " + value);
							}
						} else {
							Window.alert("La valeur " + value
									+ " n'est pas compatible : ");
							nameCell.clearViewData(IHM.KEY_PROVIDER
									.getKey(object));
						}
						// Redraw the table with the new data.
						// Example.dataProvider.refresh();
						IHM.table.redrawRow(index);
					}
				});
	}

}
