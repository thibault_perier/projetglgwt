package fr.ensai.projet.client.dialogBox;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import fr.ensai.projet.client.IHM;

/**
 * Classe permettant a un client d'importer son propre PCM
 *
 */
public class ImporterPcmExterne {

	/*
	 * Attributs
	 */
	/*
	 * Conteneur de la fenetre Pop up qui s'affiche lorsque l'on souhaite
	 * supprimer une feature.
	 */
	private final DialogBox dialogBox;
	/*
	 * Contenu de la dialogBox.
	 */

	private final HorizontalPanel dialogContents;
	private final HorizontalPanel buttonsPanel = new HorizontalPanel();
	private final VerticalPanel verticalPanelPourImport = new VerticalPanel();
	public static final String UPLOAD_ACTION_URL = GWT.getModuleBaseURL()
			+ "FileUploadServlet";
	private String fileName = "";
	private final FormPanel form = new FormPanel();

	public ImporterPcmExterne() {
		dialogBox = new DialogBox();
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.ensureDebugId("ImporterDialogBox");
		dialogBox.setTitle("Importer PCM");
		dialogContents = new HorizontalPanel();
		dialogContents.setSpacing(10);
		dialogBox.setWidget(dialogContents);
		addForm();
	}

	/*
	 * Accesseur
	 */
	public PopupPanel getDialogBox() {
		return dialogBox;
	}

	/**
	 * Methode d'afficher un formulaire
	 */
	public void addForm() {
		// Create a FormPanel and point it at a service.
		form.setAction(UPLOAD_ACTION_URL);

		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);

		form.setWidget(verticalPanelPourImport);

		final FileUpload upload;

		// Create a FileUpload widget.
		upload = new FileUpload();
		upload.setName("uploadFormElement");
		verticalPanelPourImport.add(upload);
		// Add a 'submit' button.
		buttonsPanel.add(new Button("Submit", new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (!upload.getFilename().equals("")) {
					form.submit();
					dialogBox.hide();
				}
			}
		}));

		buttonsPanel.add(new Button("Cancel", new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		}));

		verticalPanelPourImport.add(buttonsPanel);

		form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
			public void onSubmitComplete(SubmitCompleteEvent event) {
				fileName = upload.getFilename() + "";
				Logger logger = Logger.getLogger("Import du PCM success");
				logger.log(Level.INFO, fileName.substring(12));
				Window.alert(event.getResults());
				if (fileName.length() > 12) {
					IHM.importerPcm(fileName.substring(12));
				}
			}
		});
		dialogContents.add(form);
	}

}
