package fr.ensai.projet.client.dialogBox;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.client.column.IColumn;
import fr.ensai.projet.shared.metier.SharedFeature;

/**
 * Classe permettant de renommer une feature
 * 
 */
public class RenommerFeature {

	/*
	 * Attributs
	 */

	/*
	 * Conteneur de la fenetre Pop up qui s'affiche lorsque l'on souhaite
	 * supprimer une feature.
	 */
	private final DialogBox dialogBox;
	private final TextBox textbox;
	/*
	 * Contenu de la dialogBox.
	 */
	private final VerticalPanel dialogContents = new VerticalPanel();
	private final HorizontalPanel buttonsPanel = new HorizontalPanel();
	private final ValueListBox<SharedFeature> lisBoxAvailableFeature = new ValueListBox<SharedFeature>(
			new Renderer<SharedFeature>() {

				@Override
				public String render(SharedFeature object) {
					String render = "";
					if (object != null) {
						if (object.isKing()) {
							render = object.getName().toUpperCase() + "(fam.)";
						} else {
							render = object.getName().toLowerCase();
						}
					}
					return render;
				}

				@Override
				public void render(SharedFeature object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);

				}
			});

	/*
	 * Accesseur
	 */

	public PopupPanel getDialogBox() {
		return dialogBox;
	}

	/*
	 * Constructeur
	 */
	public RenommerFeature() {
		dialogBox = new DialogBox();
		dialogBox.setHTML("Changement de nom");
		textbox = new TextBox();
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.ensureDebugId("modifierFeatureDialogBox");
		dialogBox.setTitle("Renommer Feature");
		dialogBox.setWidget(dialogContents);
		dialogContents.add(lisBoxAvailableFeature);
		dialogContents.add(textbox);
		getAllFeatures();
		addButtons();

	}

	/**
	 * Creation et ajout du button Cancel (button d'annulation)
	 * 
	 * @param buttonsPanel
	 */
	private void ajouterBoutonAnnuler() {
		Button button = new Button("Cancel");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});
		buttonsPanel.add(button);
	}

	/*
	 * Méthodes fonctionnelles
	 */

	/**
	 * Creation et ajout du button remove (button de validation)
	 * 
	 * @param buttonsPanel
	 */
	private void ajouterBoutonModifier() {
		Button button = new Button("Appliquer");
		button.addClickHandler(new ClickHandler() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(ClickEvent event) {
				SharedFeature featureAModifier = lisBoxAvailableFeature
						.getValue();
				if (featureAModifier != null && textbox.getText().length() > 0) {
					IHM.PCM.findFeatureById(featureAModifier.getId()).setName(
							textbox.getText());
					IColumn columnModif = null;
					for (IColumn myColumn : IHM.myColumns) {
						if (!featureAModifier.isKing()) {
							if (myColumn.getSharedFeature().equals(
									featureAModifier)) {
								columnModif = myColumn;

								IHM.table.removeColumn(columnModif
										.getNameColumn());
								IHM.table.insertColumn(CreerFeature
										.positionColumn(columnModif
												.getSharedFeature()),
										columnModif.getNameColumn(), "["
												+ featureAModifier.getParent()
														.getName()
														.toUpperCase()
												+ "] "
												+ featureAModifier.getName()
														.toLowerCase());
							}
						}
					}
				}
				dialogBox.hide();
			}
		});
		buttonsPanel.add(button);
	}

	/**
	 * Ajout des boutons cancel et create dans la boite de dialogue.
	 */
	private void addButtons() {
		ajouterBoutonAnnuler();
		ajouterBoutonModifier();
		dialogContents.add(buttonsPanel);
	}

	/*
	 * Methode interne
	 */
	/**
	 * Options disponible dans la listBox du choix des features a supprimer.
	 */
	private void getAllFeatures() {
		Set<SharedFeature> allFeatures = new HashSet<SharedFeature>();
		allFeatures.addAll(IHM.PCM.getFeaturesShared());
		// allFeatures.addAll(IHM.PCM.getKingfeatures());
		lisBoxAvailableFeature.setAcceptableValues(allFeatures);
	}

}
