package fr.ensai.projet.client.dialogBox;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ValueListBox;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.client.column.IColumn;
import fr.ensai.projet.shared.metier.SharedFeature;

/**
 * Classe permettant de supprimer une feature du tableau et du PCM courant
 *
 */
public class SupprimerFeature {

	/*
	 * Attributs
	 */
	/*
	 * Conteneur de la fenetre Pop up qui s'affiche lorsque l'on souhaite
	 * supprimer une feature.
	 */
	private final DialogBox dialogBox;
	/*
	 * Contenu de la dialogBox.
	 */
	private final HorizontalPanel dialogContents;
	private final HorizontalPanel buttonsPanel = new HorizontalPanel();
	private final ValueListBox<SharedFeature> lisBoxAvailableFeature = new ValueListBox<SharedFeature>(
			new Renderer<SharedFeature>() {

				@Override
				public String render(SharedFeature object) {
					String render = "";
					if (object != null) {
						if (object.isKing()) {
							render = "[" + object.getName().toUpperCase() + "]";
						} else {
							render = object.getName().toLowerCase();
						}
					}
					return render;
				}

				@Override
				public void render(SharedFeature object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);

				}
			});

	/*
	 * Constructeur
	 */
	public SupprimerFeature() {
		dialogBox = new DialogBox();
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.ensureDebugId("RemoveFeatureDialogBox");
		dialogBox.setTitle("Remove Feature");
		dialogContents = new HorizontalPanel();
		dialogContents.setSpacing(10);
		dialogBox.setWidget(dialogContents);
		dialogContents.add(lisBoxAvailableFeature);
		getAllFeatures();
		addButtons();

	}

	/**
	 * Options disponible dans la listBox du choix des features a supprimer.
	 */
	private void getAllFeatures() {
		Set<SharedFeature> features = IHM.PCM.getFeaturesShared();
		Set<SharedFeature> kingFeatures = getFamille();
		Set<SharedFeature> allFeatures = new HashSet<SharedFeature>();
		allFeatures.addAll(features);
		allFeatures.addAll(kingFeatures);

		lisBoxAvailableFeature.setAcceptableValues(allFeatures);
	}

	/*
	 * Accesseur
	 */
	public PopupPanel getDialogBox() {
		return dialogBox;
	}

	/*
	 * Methode de gestion interne de la classe
	 */
	/**
	 * Creation et ajout du button Cancel (button d'annulation)
	 * 
	 * @param buttonsPanel
	 */
	private void addCancelButton() {
		Button button = new Button("Cancel");
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});

		buttonsPanel.add(button);
	}

	/**
	 * Ajout des boutons cancel et create dans la boite de dialogue.
	 */
	private void addButtons() {
		addCancelButton();
		addRemoveButton();
		dialogContents.add(buttonsPanel);
	}

	/**
	 * Creation et ajout du button remove (button de validation)
	 * 
	 * @param buttonsPanel
	 */
	private void addRemoveButton() {
		Button button = new Button("Remove");
		button.addClickHandler(new ClickHandler() {

			@SuppressWarnings("unchecked")
			@Override
			public void onClick(ClickEvent event) {

				SharedFeature featureRemove = lisBoxAvailableFeature.getValue();
				if (featureRemove != null) {
					if (!featureRemove.isKing()) {
						IColumn columnRemove = null;
						for (IColumn myColumn : IHM.myColumns) {
							if (myColumn.getSharedFeature().equals(
									featureRemove)) {
								columnRemove = myColumn;
								break;
							}
						}
						IHM.PCM.removeFeature(columnRemove.getSharedFeature());
						IHM.myColumns.remove(columnRemove);
						IHM.table.removeColumn(columnRemove.getNameColumn());
					} else {
						Set<IColumn> columnsRemove = new HashSet<IColumn>();
						for (IColumn myColumn : IHM.myColumns) {
							if (myColumn.getSharedFeature().getParent() != null
									&& myColumn.getSharedFeature().getParent()
											.equals(featureRemove)) {
								columnsRemove.add(myColumn);
							}
						}
						for (IColumn myColumn2 : columnsRemove) {

							IHM.PCM.removeFeature(myColumn2.getSharedFeature());
							IHM.table.removeColumn(myColumn2.getNameColumn());
						}
						IHM.myColumns.removeAll(columnsRemove);
						IHM.PCM.removeFeature(featureRemove);
					}
					dialogBox.hide();
					IHM.updateVariables();
				}
			}
		});
		buttonsPanel.add(button);
	}

	/**
	 * Options disponible dans la listBox du choix de la Famille.
	 * 
	 * @return Set<SharedFeature>
	 */
	private Set<SharedFeature> getFamille() {
		Set<SharedFeature> featuresKing = new HashSet<SharedFeature>();

		for (SharedFeature feature : IHM.PCM.getKingfeatures()) {
			featuresKing.add((SharedFeature) feature);
		}
		return featuresKing;
	}
}
