package fr.ensai.projet.client;

import java.util.Set;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.smartgwt.client.widgets.HTMLFlow;

import fr.ensai.projet.shared.metier.SharedCommentaire;
import fr.ensai.projet.shared.metier.SharedProduct;

/**
 * Gestionnaire des commentaires.
 * 
 */
public class Commentaires extends VerticalPanel {

	/*
	 * Attributs
	 */
	public final TextArea textArea = new TextArea();
	private final HTMLFlow htmlFlow = new HTMLFlow();
	private final Label commentLabel;

	/*
	 * Constructeur
	 */

	/**
	 * Constructeur
	 * 
	 * @param product
	 *            SharedProduct
	 */
	public Commentaires(SharedProduct product) {
		this.setSpacing(10);
		final SharedProduct product2 = product;
		this.add(new Label("Insérez votre commentaire ici :"));
		textArea.setVisibleLines(5);
		textArea.setSize("100px", "50px");
		this.add(textArea);
		commentLabel = new Label("Press CTRL+ENTER to add a comment");
		this.add(commentLabel);
		textArea.setVisible(false);

		// Press Insert pour ajouter un commentaire
		textArea.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.isControlKeyDown()) {
					if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
						if (!textArea.getText().equals("")) {
							product2.getCommentairesShared().add(
									new SharedCommentaire(textArea.getText(),
											IHM.PCM.getAuthor(), product2));
							showComment(product2);
							textArea.setText("");
						}
					}
				}
			}

		});

	}

	/*
	 * Methodes fonctionnelles de la classe
	 */

	/**
	 * Affiche les commentaire d'un produit.
	 * 
	 * @param product
	 *            SharedProduct
	 */
	public void showComment(SharedProduct product) {
		Set<SharedCommentaire> commentaires = product.getCommentairesShared();
		textArea.setVisible(true);
		if (commentaires.size() > 0) {
			String contents = "<h3>Voici les commentaires déjà édités sur ce produit :</h3>";
			htmlFlow.clear();
			for (SharedCommentaire commentaire : commentaires) {
				contents += "Le " + commentaire.getDate() + " par "
						+ commentaire.getAuteur() + " :<br/>" + "<p>"
						+ commentaire.getTexte() + "</p></br>";
			}
			this.add(htmlFlow);
			htmlFlow.setContents(contents);
		}
	};

	/**
	 * Cache les commentaires.
	 */
	public void hideComment() {
		textArea.setVisible(false);
		this.removeFromParent();
	}

	/**
	 * Change le nom du button de showComments a HideComments en fonction de la
	 * variable show.
	 * 
	 * @param boutonVoirCommentaires
	 * @param show
	 */
	public void changeButtonName(Button boutonVoirCommentaires, boolean show) {
		if (show) {
			boutonVoirCommentaires.setTitle("Lire les commentaires");
		} else {
			boutonVoirCommentaires.setTitle("Masquer les commentaires");
		}
	}
}
