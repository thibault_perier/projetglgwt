package fr.ensai.projet.client.dialogBox;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

import fr.ensai.projet.client.IHM;
import fr.ensai.projet.client.column.ColumnEditString;
import fr.ensai.projet.client.column.ColumnSelectionEnum;
import fr.ensai.projet.client.column.IColumn;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedTypeBoolean;
import fr.ensai.projet.shared.metier.SharedTypeDate;
import fr.ensai.projet.shared.metier.SharedTypeDouble;
import fr.ensai.projet.shared.metier.SharedTypeEnum;
import fr.ensai.projet.shared.metier.SharedTypeString;

/**
 * Gestion de la creation des features.
 * 
 *
 */
public class CreerFeature {

	/*
	 * Conteneur de la fenetre Pop up qui s'affiche lorsque l'on souhaite creer
	 * une feature.
	 */
	private final DialogBox dialogBox;
	/*
	 * Contenu de la dialogBox.
	 */
	private final VerticalPanel dialogContents;
	private final ListBox listBoxAvailableTypes = new ListBox();
	private final ListBox listBoxModalities = new ListBox();
	private final HorizontalPanel namePanel = new HorizontalPanel();
	private final TextBox textBoxModality = new TextBox();
	private final Button buttonAddModality = new Button("Add modality");
	private final VerticalPanel typeVerticalPanel = new VerticalPanel();
	private final HorizontalPanel typeHorizontalPanel = new HorizontalPanel();
	private final HorizontalPanel buttonsPanel = new HorizontalPanel();
	private final HorizontalPanel addModality = new HorizontalPanel();
	private final TextBox nameCell = new TextBox();
	private final HorizontalPanel addFamille = new HorizontalPanel();
	private final Button buttonAddChild = new Button("Add child");
	private final Set<SharedFeature> selectedChilren = new HashSet<SharedFeature>();
	private final ValueListBox<SharedFeature> listBoxAvailableChild = new ValueListBox<SharedFeature>(
			new Renderer<SharedFeature>() {
				@Override
				public String render(SharedFeature object) {
					if (object == null) {
						return "";
					} else
						return object.getName();
				}

				@Override
				public void render(SharedFeature object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);
				}
			});
	private final ListBox listBoxSelectedChild = new ListBox();
	private final ValueListBox<SharedFeature> listBoxAvailableFamille = new ValueListBox<SharedFeature>(
			new Renderer<SharedFeature>() {
				@Override
				public String render(SharedFeature object) {
					if (object == null) {
						return "";
					} else
						return object.getName();
				}

				@Override
				public void render(SharedFeature object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);

				}
			});

	/*
	 * Constructeur
	 */
	public CreerFeature() {
		dialogBox = new DialogBox();
		dialogBox.setHTML("Créer une feature");
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.ensureDebugId("FeatureDialogBox");
		dialogBox.setTitle("Create Feature");
		dialogContents = new VerticalPanel();
		dialogContents.setSpacing(10);
		dialogBox.setWidget(dialogContents);
		enterTheName();
		chooseYourType();
		addButtons();
	}

	/*
	 * Methodes fonctionnelles
	 */
	/**
	 * Construction et ajout du widget permettant de nommer la feature.
	 */
	private void enterTheName() {
		String nomFeature = "Nom de la feature : ";
		Label disLabel = new Label(nomFeature);
		namePanel.add(disLabel);
		namePanel.add(nameCell);
		dialogContents.add(namePanel);

	}

	/**
	 * Ajout de la listBox permettant de choisir un type et la famille.
	 */
	private void addListBoxTypes() {
		Label listBoxLabel = new Label("Type : ");
		getOptions();
		typeHorizontalPanel.add(listBoxLabel);
		typeHorizontalPanel.add(listBoxAvailableTypes);
		typeHorizontalPanel.add(listBoxAvailableFamille);
		getFamille();
		dialogContents.add(typeHorizontalPanel);

	}

	/**
	 * Action graphiques a realiser en fonction du type selectionne.
	 */
	private void listBoxAvailableTypesAddChangeHandler() {

		listBoxAvailableTypes.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {

				if (listBoxAvailableTypes.getItemText(
						listBoxAvailableTypes.getSelectedIndex())
						.equals("Enum")) {

					typeHorizontalPanel.add(listBoxModalities);

					typeVerticalPanel.add(addModality);

				} else {
					addModality.removeFromParent();
					listBoxModalities.removeFromParent();
				}

				if (listBoxAvailableTypes.getItemText(
						listBoxAvailableTypes.getSelectedIndex()).equals(
						"Famille")) {

					typeHorizontalPanel.add(listBoxSelectedChild);

					typeVerticalPanel.add(addFamille);

					listBoxAvailableFamille.setVisible(false);

				} else {
					addFamille.removeFromParent();
					listBoxSelectedChild.removeFromParent();
					listBoxAvailableFamille.setVisible(true);
					selectedChilren.clear();
				}

			}

		});

	}

	/**
	 * Creation et ajout des composants graphiques correspondant au choix des
	 * modalites d'une Feature Enum.
	 */
	private void addChoiceOfModalitiesEnum() {
		listBoxModalities.setVisibleItemCount(3);
		listBoxModalities.setSize("100px", "100px");

		Label disLabel1 = new Label("Modality :");
		addModality.add(disLabel1);
		addModality.add(textBoxModality);
		addModality.add(buttonAddModality);
		dialogContents.add(typeVerticalPanel);

		buttonAddModality.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				listBoxModalities.addItem(textBoxModality.getValue());
			}
		});

		listBoxAvailableTypesAddChangeHandler();

	}

	/**
	 * Creation et ajout des composants graphiques correspondant au choix des
	 * des children d'une Feature Famille.
	 */
	private void addChoiceOfChildrenFamille() {
		listBoxSelectedChild.setVisibleItemCount(3);
		listBoxSelectedChild.setSize("60px", "60px");

		getFeatures();

		Label disLabel1 = new Label("Child :");
		addFamille.add(disLabel1);
		addFamille.add(listBoxAvailableChild);
		addFamille.add(buttonAddChild);
		dialogContents.add(typeVerticalPanel);

		buttonAddChild.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				if (selectedChilren.add(listBoxAvailableChild.getValue())) {
					listBoxSelectedChild.addItem(listBoxAvailableChild
							.getValue().getName());
				}
				// listBoxAvailableChild.removeItem(listBoxAvailableChild.getSelectedIndex());
				// listBoxSelectedChild.setValue(listBoxAvailableChild.getValue());

			}
		});

		listBoxAvailableTypesAddChangeHandler();

	}

	/**
	 * Construction et ajout du widget de choisir le Type de la feature.
	 */
	private void chooseYourType() {
		typeHorizontalPanel.setSpacing(10);

		addListBoxTypes();

		addChoiceOfModalitiesEnum();

		addChoiceOfChildrenFamille();

	}

	/**
	 * Ajout des boutons cancel et create dans la boite de dialogue.
	 */
	private void addButtons() {
		addCancelButton();
		addCreateButton();
		dialogContents.add(buttonsPanel);
	}

	/**
	 * Options disponible dans la listBox du choix du type.
	 */
	private void getOptions() {
		listBoxAvailableTypes.addItem("Boolean");
		listBoxAvailableTypes.addItem("Date");
		listBoxAvailableTypes.addItem("Double");
		listBoxAvailableTypes.addItem("Enum");
		listBoxAvailableTypes.addItem("String");
		listBoxAvailableTypes.addItem("Famille");
	}

	/**
	 * Options disponible dans la listBox du choix du child.
	 */
	private void getFeatures() {

		Set<SharedFeature> features = IHM.PCM.getFeaturesShared();
		Set<SharedFeature> acceptedValues = new HashSet<SharedFeature>();
		listBoxAvailableFamille.setValue(null);
		for (SharedFeature feature : features) {
			if (feature.getParent() == null) {
				acceptedValues.add(feature);
			}
		}
		listBoxAvailableChild.setAcceptableValues(acceptedValues);

	}

	/**
	 * Options disponible dans la listBox du choix de la Famille.
	 */
	private void getFamille() {

		Set<SharedFeature> featuresKing = new HashSet<SharedFeature>();

		for (SharedFeature feature : IHM.PCM.getKingfeatures()) {

			if (featuresKing.add(feature)) {
				listBoxAvailableFamille.setValue((SharedFeature) feature);

			}
		}

	}

	/**
	 * Creation et ajout du button Cancel (button d'annulation)
	 * 
	 * @param buttonsPanel
	 */
	private void addCancelButton() {
		Button button = new Button("Cancel");
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});

		buttonsPanel.add(button);
	}

	/**
	 * Position a laquelle doit etre introduite une colonne correspondant a une
	 * feature. Les features de la meme famille sont groupees. Les features sans
	 * famille sont situees apres la colonne NAME.
	 * 
	 * @param feature
	 *            SharedFeature
	 * @return int
	 */
	@SuppressWarnings("unchecked")
	public static int positionColumn(SharedFeature feature) {

		if (feature.getParent() == null) {
			return 1;
		} else {
			for (IColumn myColumn : IHM.myColumns) {

				if (feature.getParent() != null
						&& myColumn.getSharedFeature().getParent() != null
						&& feature.getParent().getChildren().size() != 1
						&& myColumn.getSharedFeature().getParent()
								.equals(feature.getParent())) {

					Integer index = IHM.table.getColumnIndex(myColumn
							.getNameColumn());
					if (index != -1) {
						return index;
					}
				}
			}
			return 1;
		}

	}

	/**
	 * Creation et ajout du button Create (button de validation)
	 */
	private void addCreateButton() {
		Button button = new Button("Create");
		button.addClickHandler(new ClickHandler() {
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(ClickEvent event) {
				if (nameCell.getText().trim().length() > 0) {

					String selected = listBoxAvailableTypes
							.getItemText(listBoxAvailableTypes
									.getSelectedIndex());
					SharedFeature feature = null;
					SharedFeature selectedFamille = listBoxAvailableFamille
							.getValue();

					switch (selected) {
					case "Boolean":
						feature = new SharedFeature(nameCell.getText(),
								new SharedTypeBoolean(), IHM.PCM);
						ColumnEditString col1 = new ColumnEditString(
								(SharedFeature) feature);

						if (selectedFamille != null) {
							selectedFamille.addChild((SharedFeature) feature);
							IHM.PCM.addFeature(selectedFamille);
							IHM.myColumns.add(col1);

							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col1.getNameColumn(), "["
											+ feature.getParent().getName()
													.toUpperCase()
											+ "] "
											+ col1.getSharedFeature().getName()
													.toLowerCase());

						} else {
							IHM.myColumns.add(col1);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col1.getNameColumn(), col1
											.getSharedFeature().getName()
											.toLowerCase());
						}

						break;
					case "Date":
						feature = new SharedFeature(nameCell.getText(),
								new SharedTypeDate(), IHM.PCM);

						ColumnEditString col11111 = new ColumnEditString(
								(SharedFeature) feature);

						if (selectedFamille != null) {
							selectedFamille.addChild((SharedFeature) feature);
							IHM.PCM.addFeature(selectedFamille);
							IHM.myColumns.add(col11111);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col11111.getNameColumn(), "["
											+ feature.getParent().getName()
													.toUpperCase()
											+ "] "
											+ col11111.getSharedFeature()
													.getName().toLowerCase());
						} else {
							IHM.myColumns.add(col11111);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col11111.getNameColumn(), col11111
											.getSharedFeature().getName()
											.toLowerCase());

						}

						break;
					case "Double":
						feature = new SharedFeature(nameCell.getText(),
								new SharedTypeDouble(), IHM.PCM);

						ColumnEditString col1111 = new ColumnEditString(
								(SharedFeature) feature);

						if (selectedFamille != null) {
							selectedFamille.addChild((SharedFeature) feature);
							IHM.PCM.addFeature(selectedFamille);
							IHM.myColumns.add(col1111);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col1111.getNameColumn(), "["
											+ feature.getParent().getName()
													.toUpperCase()
											+ "] "
											+ col1111.getSharedFeature()
													.getName().toLowerCase());
						} else {
							IHM.myColumns.add(col1111);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col1111.getNameColumn(), col1111
											.getSharedFeature().getName()
											.toLowerCase());
						}
						break;
					case "Enum":
						Set<String> values = new HashSet<String>();
						for (int j = 0; j < listBoxModalities.getItemCount(); j++) {
							values.add(listBoxModalities.getItemText(j));
						}

						feature = new SharedFeature(nameCell.getText(),
								new SharedTypeEnum(values), IHM.PCM);
						ColumnSelectionEnum col11 = new ColumnSelectionEnum(
								(SharedFeature) feature);

						if (selectedFamille != null) {

							selectedFamille.addChild((SharedFeature) feature);
							IHM.PCM.addFeature(selectedFamille);
							IHM.myColumns.add(col11);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col11.getNameColumn(), "["
											+ feature.getParent().getName()
													.toUpperCase()
											+ "] "
											+ col11.getSharedFeature()
													.getName().toLowerCase());

						} else {
							IHM.myColumns.add(col11);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col11.getNameColumn(), col11
											.getSharedFeature().getName()
											.toLowerCase());
						}

						break;
					case "String":
						feature = new SharedFeature(nameCell.getText(),
								new SharedTypeString(), IHM.PCM);

						ColumnEditString col111111 = new ColumnEditString(
								(SharedFeature) feature);

						if (selectedFamille != null) {
							selectedFamille.addChild((SharedFeature) feature);
							IHM.PCM.addFeature(selectedFamille);
							IHM.myColumns.add(col111111);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col111111.getNameColumn(), "["
											+ feature.getParent().getName()
													.toUpperCase()
											+ "] "
											+ col111111.getSharedFeature()
													.getName().toLowerCase());
						} else {
							IHM.myColumns.add(col111111);
							IHM.table.insertColumn(
									positionColumn((SharedFeature) feature),
									col111111.getNameColumn(), col111111
											.getSharedFeature().getName()
											.toLowerCase());

						}

						break;
					case "Famille":
						feature = new SharedFeature(nameCell.getText(), IHM.PCM);

						for (IColumn myColumn : IHM.myColumns) {
							if (selectedChilren.contains(myColumn
									.getSharedFeature())) {
								// delete from table
								IHM.table.removeColumn(myColumn.getNameColumn());
								// Modify feature
								feature.addChild(myColumn.getSharedFeature());
								// insert new feature
								IHM.table
										.insertColumn(
												positionColumn((SharedFeature) feature),
												myColumn.getNameColumn(),
												"["
														+ myColumn
																.getSharedFeature()
																.getParent()
																.getName()
																.toUpperCase()
														+ "] "
														+ myColumn
																.getSharedFeature()
																.getName()
																.toLowerCase());

							}
						}
						break;

					default:
						return;

					}

					IHM.PCM.addFeature(feature);
					IHM.updateVariables();
					dialogBox.hide();
				}
			}
		});

		buttonsPanel.add(button);
	}

	public DialogBox getDialogBox() {
		return dialogBox;
	}

}
