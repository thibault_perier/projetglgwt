package fr.ensai.projet.client.dialogBox;

import java.io.IOException;
import java.util.Set;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.ValueListBox;

import fr.ensai.projet.client.IHM;

public class ChargerPcm {
	/*
	 * Conteneur de la fenetre Pop up qui s'affiche lorsque l'on souhaite
	 * changer la famille d'une feature.
	 */
	private final DialogBox dialogBox;
	/*
	 * Contenu de la dialogBox.
	 */
	private final HorizontalPanel dialogContents;
	private final HorizontalPanel buttonsPanel = new HorizontalPanel();
	private final ValueListBox<String> lisBoxAvailablePcm = new ValueListBox<String>(
			new Renderer<String>() {

				@Override
				public String render(String object) {
					String render = "";
					if (object != null) {
						render = object.toLowerCase();

					}
					return render;
				}

				@Override
				public void render(String object, Appendable appendable)
						throws IOException {
					String s = render(object);
					appendable.append(s);
				}
			});

	/*
	 * Accesseur
	 */
	public PopupPanel getDialogBox() {
		return dialogBox;
	}

	/**
	 * Constructeurs permettant d'initialiser la dialogbox avec la liste des PCM
	 * disponibles
	 * 
	 * @param nomPcmDispos
	 *            Set<String>
	 */
	public ChargerPcm(Set<String> nomPcmDispos) {
		dialogBox = new DialogBox();
		dialogBox.setGlassEnabled(true);
		dialogBox.setAnimationEnabled(true);
		dialogBox.ensureDebugId("ChargerPCMDialogBox");
		dialogBox.setTitle("Remove Feature");
		dialogContents = new HorizontalPanel();
		dialogContents.setSpacing(10);
		dialogContents.add(lisBoxAvailablePcm);
		dialogBox.setWidget(dialogContents);
		lisBoxAvailablePcm.setAcceptableValues(nomPcmDispos);
		addButtons();
	}

	/**
	 * Creation et ajout du button Cancel (button d'annulation)
	 * 
	 * @param buttonsPanel
	 */
	private void addCancelButton() {
		Button button = new Button("Cancel");
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		});
		buttonsPanel.add(button);
	}

	/**
	 * Ajout des boutons cancel et create dans la boite de dialogue.
	 */
	private void addButtons() {
		addCancelButton();
		addChargerButton();
		dialogContents.add(buttonsPanel);
	}

	/**
	 * Creation et ajout du button modifier (button de validation)
	 * 
	 * @param buttonsPanel
	 */
	private void addChargerButton() {
		Button button = new Button("Charger");
		button.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				String pcmChoisi = lisBoxAvailablePcm.getValue();
				if (pcmChoisi != null) {
					IHM.importerPcm(pcmChoisi);
					dialogBox.hide();
				}
				dialogBox.hide();
			}
		});

		buttonsPanel.add(button);
	}
}
