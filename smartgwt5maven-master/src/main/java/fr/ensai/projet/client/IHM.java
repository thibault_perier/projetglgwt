package fr.ensai.projet.client;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.view.client.CellPreviewEvent;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;
import com.smartgwt.client.widgets.HTMLFlow;

import fr.ensai.projet.client.column.ColumnEditString;
import fr.ensai.projet.client.column.ColumnName;
import fr.ensai.projet.client.column.ColumnSelectionEnum;
import fr.ensai.projet.client.column.IColumn;
import fr.ensai.projet.client.dialogBox.CreerFeature;
import fr.ensai.projet.client.service.GreetingService;
import fr.ensai.projet.client.service.GreetingServiceAsync;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedPcm;
import fr.ensai.projet.shared.metier.SharedProduct;
import fr.ensai.projet.shared.metier.SharedTypeEnum;

/**
 * Interface GWT
 */
public class IHM implements EntryPoint {

	/*
	 * The list of data to display.
	 */
	public static SharedPcm PCM;
	private static String nomDuPcm = "";
	private static String auteurDuPcm = "";
	private static String descriptionDuPcm = "";

	private static int varNbVariable;
	private static int varNbLigne;
	private static int varNbFamille;
	private static Commande commande = new Commande();
	// utiliser HTMLFlow
	static HTMLFlow nbVariable = new HTMLFlow();
	static HTMLFlow nbLigne = new HTMLFlow();
	static HTMLFlow nbFamille = new HTMLFlow();
	/*
	 * En-tête du PCM
	 */
	private static final HTMLFlow htmlFlow = new HTMLFlow();

	/**
	 * The key provider that allows us to identify Contacts even if a field
	 * changes. We identify contacts by their unique ID.
	 */
	public static ProvidesKey<SharedProduct> KEY_PROVIDER = new ProvidesKey<SharedProduct>() {
		@Override
		public Object getKey(SharedProduct item) {
			return item.getId();
		}
	};
	// CellTable custom UI resource
	private static CellTable.Resources tableRes = GWT.create(TableRes.class);
	// Create a CellTable with a key provider.
	public static CellTable<SharedProduct> table = new CellTable<SharedProduct>(
			15, tableRes, KEY_PROVIDER);
	// Create a data provider.
	public static ListDataProvider<SharedProduct> dataProvider = new ListDataProvider<SharedProduct>();
	// Les colonnes de la table
	public static List<IColumn> myColumns = new ArrayList<IColumn>();

	/*
	 * ELEMENTS DE L'INTERFACE
	 */

	/*
	 * Elements generaux
	 */
	// Le Dock Panel permettant de diviser la Page (North/SOuth/center ...)
	public static final DockPanel dock = new DockPanel();
	// Barre de défilement pour la le PCM
	private static final ScrollPanel scrollerPourPCM = new ScrollPanel(table);
	// Gestionnaire de commentaire
	public static Commentaires comment = null;
	private static MenuItem commentairesProduct = null;
	private static String show = "Afficher les commentaires";
	private static String hide = "Masquer les commentaires";
	private static WidgetEditable textAreaName;
	private static WidgetEditable tAreaAuteur;
	private static WidgetEditable tAreaDescription;

	// Panel vertical conenant la table et les button
	private static final VerticalPanel verticalPanelPourPCM = new VerticalPanel();

	@Override
	public void onModuleLoad() {
		initialiserPCM();

		// Create a Dock Panel
		dock.setStyleName("dockpanel");
		dock.setWidth("100%");
		dock.setSpacing(4);
		dock.setHorizontalAlignment(DockPanel.ALIGN_CENTER);
		dock.setVerticalAlignment(DockPanel.ALIGN_TOP);

		/*
		 * Initialisation de l'en-tete, les ajouts consecutifs au "NORTH" se
		 * verront de haut en bas
		 */
		dock.add(new HTML("<h1>Projet Génie Logiciel</h1>"), DockPanel.NORTH);

		/*
		 * Initialistaion du pied de page, les ajouts consecutifs au "SOUTH" se
		 * verront de bas en haut
		 */
		dock.add(
				new HTML(
						"<h4 style='color:#5d5d5d;''>Ensai - 3A Sid - 2014/2015.</h4>"),
				DockPanel.SOUTH);

		htmlFlow.setContents("En attente d'informations");
		verticalPanelPourPCM.setHorizontalAlignment(VerticalPanel.ALIGN_CENTER);
		verticalPanelPourPCM.add(htmlFlow);

		/*
		 * Placement des boutons dans une barre en haut
		 */

		/*
		 * Bouton "Produit..."
		 */
		MenuBar menuProduct = new MenuBar(true);
		MenuItem ajoutProduct = new MenuItem("Ajouter",
				commande.getAjouterProduct());
		MenuItem suppressionProduct = new MenuItem("Supprimer",
				commande.getSupprimerProduct());
		commentairesProduct = new MenuItem(show, commande.getLireCommentaire());
		menuProduct.addItem(ajoutProduct);
		menuProduct.addItem(suppressionProduct);
		menuProduct.addItem(commentairesProduct);

		/*
		 * Bouton "Feature..."
		 */
		MenuBar menuFeature = new MenuBar(true);
		MenuItem ajoutFeature = new MenuItem("Ajouter",
				commande.getAjouterFeature());
		MenuItem suppressionFeature = new MenuItem("Supprimer",
				commande.getSupprimerFeature());
		MenuItem modifierFamille = new MenuItem("Modifier famille",
				commande.getChangerFamille());
		MenuItem modificationNom = new MenuItem("Renommer",
				commande.getModifierNom());
		menuFeature.addItem(ajoutFeature);
		menuFeature.addItem(suppressionFeature);
		menuFeature.addItem(modifierFamille);
		menuFeature.addItem(modificationNom);

		/*
		 * Bouton "Outils..."
		 */
		MenuBar menuOutils = new MenuBar(true);
		MenuItem sauvegarde = new MenuItem("Sauvegarder",
				commande.getSauvegarder());
		MenuItem exportation = new MenuItem("Exporter (xml)",
				commande.getExporter());
		MenuItem importation = new MenuItem("Importer", commande.getImporter());
		MenuItem chargement = new MenuItem("Charger", commande.getCharger());
		menuOutils.addItem(exportation);
		menuOutils.addItem(sauvegarde);
		menuOutils.addItem(importation);
		menuOutils.addItem(chargement);

		MenuBar menuGeneral = new MenuBar();
		menuGeneral.setWidth("500px");
		menuGeneral.addItem("Produits...", menuProduct);
		menuGeneral.addItem("Features...", menuFeature);
		menuGeneral.addItem("Outils...", menuOutils);

		/*
		 * Placement de la table du PCM
		 */
		scrollerPourPCM.setWidth("100%");
		dock.add(scrollerPourPCM, DockPanel.EAST);
		scrollerPourPCM.setStyleName("pcmMargin");
		verticalPanelPourPCM.add(menuGeneral);
		verticalPanelPourPCM.add(scrollerPourPCM);
		dock.add(verticalPanelPourPCM, DockPanel.CENTER);

		RootPanel.get().add(dock);

		// Placement des zones pour modifier le titre, l'auteur et la
		// description du PCM
		VerticalPanel verticalPanelPourCaracteristiques = new VerticalPanel();
		verticalPanelPourCaracteristiques.setWidth("30%");
		verticalPanelPourCaracteristiques
				.setHorizontalAlignment(VerticalPanel.ALIGN_LEFT);
		verticalPanelPourCaracteristiques.add(new HTML(
				"<h3>Caractéristiques du PCM</h3>"));
		textAreaName = new WidgetEditable(1);
		verticalPanelPourCaracteristiques.add(textAreaName);
		tAreaAuteur = new WidgetEditable(2);
		verticalPanelPourCaracteristiques.add(tAreaAuteur);
		tAreaDescription = new WidgetEditable(3);
		verticalPanelPourCaracteristiques.add(tAreaDescription);
		dock.add(verticalPanelPourCaracteristiques, DockPanel.WEST);

		verticalPanelPourCaracteristiques.add(nbVariable);
		verticalPanelPourCaracteristiques.add(nbLigne);
		verticalPanelPourCaracteristiques.add(nbFamille);

		table.addCellPreviewHandler(new CellPreviewEvent.Handler<SharedProduct>() {
			@Override
			public void onCellPreview(CellPreviewEvent<SharedProduct> event) {
				boolean isClick = "click".equals(event.getNativeEvent()
						.getType());
				if (isClick && comment != null && comment.textArea.isVisible()) {
					comment.hideComment();
					modifierTexteCommentaires();
				}
			}
		});
	}

	/**
	 * Methode permettant d'initialiser un pcm pour le premier lancement de la
	 * page
	 */
	private void initialiserPCM() {
		GreetingServiceAsync greetingSvc = GWT.create(GreetingService.class);
		greetingSvc.greetServer2(new AsyncCallback<SharedPcm>() {
			Logger logger = Logger.getLogger("Import du PCM");

			@Override
			public void onSuccess(SharedPcm arg0) {
				logger.log(Level.INFO, "sucess");
				PCM = arg0;
				textAreaName.setTexte(PCM.getName(), 1);
				tAreaAuteur.setTexte(PCM.getAuthor(), 2);
				tAreaDescription.setTexte(PCM.getDescription(), 3);
				myColumns.clear();
				dataProvider.addDataDisplay(table);
				table.setPageSize(Integer.MAX_VALUE);
				afficherPcm(arg0);
			}

			@Override
			public void onFailure(Throwable arg0) {
				Window.alert("Erreur lors de l'initialisation - impossible de se connecter au serveur");
				logger.log(Level.SEVERE, "failure");
			}
		});
	}

	/**
	 * Methode permettant d'afficher un PCM en mettant a jour la table
	 * 
	 * @param pcm
	 *            SharedPcm
	 */
	public static void afficherPcm(SharedPcm pcm) {
		updateVariables();
		List<SharedFeature> featuresWithoutKing = new ArrayList<SharedFeature>();
		Set<SharedFeature> kingFeatures = new HashSet<SharedFeature>();

		ColumnName nameColumn = new ColumnName();
		table.insertColumn(0, nameColumn.getNameColumn(), "Identifiant");

		for (SharedFeature feature : pcm.getFeaturesShared()) {

			if (!feature.isKing() && feature.getParent() != null) {
				kingFeatures.add(feature.getParent());
			} else if (!feature.isKing() && feature.getParent() == null) {
				featuresWithoutKing.add(feature);
			}
		}

		for (SharedFeature header : kingFeatures) {
			for (SharedFeature feat : header.getChildren()) {
				if (feat.getType() instanceof SharedTypeEnum) {
					ColumnSelectionEnum col1 = new ColumnSelectionEnum(feat);
					myColumns.add(col1);
					table.insertColumn(
							CreerFeature.positionColumn((SharedFeature) feat),
							col1.getNameColumn(), "["
									+ header.getName().toUpperCase()
									+ "] "
									+ col1.getSharedFeature().getName()
											.toLowerCase());

				} else {
					ColumnEditString col1 = new ColumnEditString(feat);
					myColumns.add(col1);
					table.insertColumn(
							CreerFeature.positionColumn((SharedFeature) feat),
							col1.getNameColumn(), "["
									+ header.getName().toUpperCase()
									+ "] "
									+ col1.getSharedFeature().getName()
											.toLowerCase());
				}

			}
		}
		for (SharedFeature feature : featuresWithoutKing) {
			if (feature.getType() instanceof SharedTypeEnum) {
				ColumnSelectionEnum col1 = new ColumnSelectionEnum(feature);
				myColumns.add(col1);
				table.insertColumn(
						CreerFeature.positionColumn((SharedFeature) feature),
						col1.getNameColumn(), col1.getSharedFeature().getName()
								.toLowerCase());

			} else {
				ColumnEditString col1 = new ColumnEditString(feature);
				myColumns.add(col1);
				table.insertColumn(
						CreerFeature.positionColumn((SharedFeature) feature),
						col1.getNameColumn(), col1.getSharedFeature().getName()
								.toLowerCase());
			}
		}

		// Add the data to the data provider, which automatically pushes it to
		// the
		// widget.
		List<SharedProduct> list = dataProvider.getList();
		for (SharedProduct product : pcm.getProducts()) {
			list.add(product);
		}

		// Add it to the root panel.
		// RootPanel.get().add(table);
		table.setVisible(true);
		modifierEnTete();
	}

	/**
	 * Methode permettant d'importer un pcm avec son nom
	 * 
	 * @param fileName
	 */
	public static void importerPcm(String fileName) {
		GreetingServiceAsync greetingSvc = GWT.create(GreetingService.class);
		greetingSvc.importWithName(fileName, new AsyncCallback<SharedPcm>() {
			Logger logger = Logger.getLogger("Import du PCM uploaded");

			@Override
			public void onSuccess(SharedPcm arg0) {
				logger.log(Level.INFO, "sucess");
				PCM = arg0;
				textAreaName.setTexte(PCM.getName(), 1);
				tAreaAuteur.setTexte(PCM.getAuthor(), 2);
				tAreaDescription.setTexte(PCM.getDescription(), 3);
				myColumns.clear();
				scrollerPourPCM.removeFromParent();
				scrollerPourPCM.clear();
				table = new CellTable<SharedProduct>(15, tableRes, KEY_PROVIDER);
				dataProvider = new ListDataProvider<SharedProduct>();
				// Connect the table to the data provider.
				dataProvider.addDataDisplay(table);
				table.setPageSize(Integer.MAX_VALUE);

				if (comment != null) {
					comment.hideComment();
				}
				comment = null;
				/*
				 * Mettre un commentaire pour cette methode
				 */
				table.addCellPreviewHandler(new CellPreviewEvent.Handler<SharedProduct>() {
					@Override
					public void onCellPreview(
							CellPreviewEvent<SharedProduct> event) {
						boolean isClick = "click".equals(event.getNativeEvent()
								.getType());

						if (isClick && comment != null
								&& comment.textArea.isVisible()) {
							comment.hideComment();
							modifierTexteCommentaires();
						}
					}
				});

				afficherPcm(arg0);
				scrollerPourPCM.add(table);
				verticalPanelPourPCM.add(scrollerPourPCM);
				commentairesProduct.setText(show);
				dataProvider.refresh();
			}

			@Override
			public void onFailure(Throwable arg0) {
				logger.log(Level.SEVERE, "failure");
			}
		});

	}

	/*
	 * Methode de mofification de texte HTML
	 */

	/**
	 * Methode permettant d emofifier l'en tete du PCM (Nom, auteur,
	 * description)
	 */
	public static void modifierEnTete() {
		nomDuPcm = PCM.getName();
		auteurDuPcm = PCM.getAuthor();
		descriptionDuPcm = PCM.getDescription();
		htmlFlow.setContents("<h2>" + nomDuPcm + " [ auteur : " + auteurDuPcm
				+ " ]" + "</h2>" + "<p> Description : " + descriptionDuPcm
				+ "</p>");
	}

	/**
	 * Methode permettant de modifier les infos sur le PCM (petites
	 * statistiques)
	 */
	public static void updateVariables() {
		varNbVariable = PCM.getNbFeature();
		varNbLigne = PCM.getProducts().size();
		varNbFamille = PCM.getKingfeatures().size();
		nbVariable.setContents("<h4>Nombre de variable(s) : " + varNbVariable
				+ "</h4>");
		nbLigne.setContents("<h4>Nombre de ligne(s) :" + varNbLigne + "</h4>");
		nbFamille.setContents("<h4>Nombre de famille(s) :" + varNbFamille
				+ "</h4>");
	}

	/**
	 * Methode permettant de modifier le texte en fonction de la présence ou non
	 * des commentaires sous le PCM
	 */
	public static void modifierTexteCommentaires() {
		if (commentairesProduct.getText().equals(show)) {
			commentairesProduct.setText(hide);
		} else {
			commentairesProduct.setText(show);
		}
	}

}