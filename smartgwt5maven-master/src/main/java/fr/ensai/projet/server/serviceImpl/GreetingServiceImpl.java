package fr.ensai.projet.server.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.JAXBException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import fr.ensai.projet.client.service.GreetingService;
import fr.ensai.projet.server.ControleurMetier;
import fr.ensai.projet.server.InitialisationPCM;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.serialisation.PcmSerializer;
import fr.ensai.projet.shared.metier.SharedPcm;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements
		GreetingService {

	/*
	 * Attributs pour la serialisation
	 */
	PcmSerializer pcmSerializer = new PcmSerializer();
	ControleurMetier controleurMetier = new ControleurMetier();

	/**
	 * Methode d'export du pcm
	 */
	@Override
	public String greetServer(SharedPcm sharedPcm)
			throws IllegalArgumentException {
		if (!(sharedPcm instanceof SharedPcm)) {
			throw new IllegalArgumentException(
					"argument to export must be a pcm");
		}
		Pcm pcm = controleurMetier.convertFromShared(sharedPcm);
		try {
			return pcmSerializer.exportPcm(pcm);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * Methode pour l'importation d'un fichier xml
	 * @throws JAXBException 
	 * @throws IOException 
	 */
	@Override
	public SharedPcm greetServer2() throws IllegalArgumentException {
		SharedPcm sp=null;
		try {
			sp = InitialisationPCM.initPCM();
		} catch (IOException | JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new IllegalArgumentException();
		}
		Pcm pcm = controleurMetier.convertFromShared(sp);
		SharedPcm sp2 = controleurMetier.convertFromServer(pcm);
		return sp2;
	}

	/**
	 * Methode pour l'importation d'un fichier xml
	 * 
	 * @throws JAXBException
	 * @throws IOException
	 */
	@Override
	public SharedPcm importWithName(String name)
			throws IllegalArgumentException {
		Pcm pcm;
		try {
			pcm = controleurMetier.importUploadedPcm(name);
		} catch (IOException | JAXBException e) {
			throw new IllegalArgumentException(
					"Echec lors du rafraichissement de l'écran");
		}
		SharedPcm sp2 = controleurMetier.convertFromServer(pcm);
		return sp2;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.ensai.projet.client.service.GreetingService#sauvegarder(fr.ensai.projet
	 * .shared.metier.SharedPcm)
	 */
	@Override
	public String sauvegarder(SharedPcm sharedPcm)
			throws IllegalArgumentException {
		if (!(sharedPcm instanceof SharedPcm)) {
			throw new IllegalArgumentException(
					"argument to export must be a pcm");
		} else {
			Pcm pcm = controleurMetier.convertFromShared(sharedPcm);
			try {
				return controleurMetier.sauvegarder(pcm);

			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "Echec de la sauvegarde : JAXBException";
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.ensai.projet.client.service.GreetingService#importerAuChoix()
	 */
	@Override
	public Set<String> listePcmPourChargement() {
		File folder = new File(PcmSerializer.getFOLDER());
		File[] listOfFiles = folder.listFiles();
		Set<String> res = new HashSet<String>();
		for (int i = 0; i < listOfFiles.length; i++) {
			res.add(listOfFiles[i].getName());
		}
		return res;
	}



}
