package fr.ensai.projet.server.conversionToClient;

import java.util.HashSet;
import java.util.Set;

import fr.ensai.projet.server.metier.Feature;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedPcm;

public class ConversionFeatureCl {

	/**
	 * Permet de convertir les features ServerPcm en features SharedPcmL
	 * @param pcm
	 * @param sharedPcm
	 */
	public static void copyFeature(Pcm pcm,SharedPcm sharedPcm){

		//Features non king with or without a king parent
		Set<SharedFeature> nonKingFeaturesShared=new HashSet<SharedFeature>();

		Set<Feature> nonKingFeaturesServer= pcm.getFeatures();

		//Features non king and without a king parent
		Set<Feature> noParentFeaturesServer=new HashSet<Feature>();

		for (Feature featureServer : nonKingFeaturesServer) {
			if (featureServer.getParent()==null) {
				noParentFeaturesServer.add(featureServer);
			}
		}

		//Copie des features sans parents
		SharedFeature tempCopie;
		for (Feature featureServer : noParentFeaturesServer) {
			tempCopie=new SharedFeature(featureServer.getId(),featureServer.getName(),null,sharedPcm);
			tempCopie.setParent(null);
			ConversionTypeCl.copyType(featureServer, tempCopie);
			nonKingFeaturesShared.add(tempCopie);

		}

		//Récupère tout les Features king
		Set<SharedFeature> kingFeaturesShared=new HashSet<SharedFeature>();
		Set<Feature> kingFeaturesServer= pcm.getKingfeatures();

		SharedFeature tempKing;
		SharedFeature tempChild;
		for (Feature header : kingFeaturesServer) {

			//Copie du parents sans enfants.
			tempKing=new SharedFeature(header.getId(),header.getName(), sharedPcm);
			kingFeaturesShared.add(tempKing);
			//Ajout à ce parents des enfants de shared			
			Set<SharedFeature> sharedChildren= new HashSet<SharedFeature>();
			Set<Feature> childrenServer=header.getChildren();

			for (Feature serverChild : childrenServer) {
				//Ajouter la copie du type pour retier l'erreur.
				tempChild=new SharedFeature(serverChild.getId(),serverChild.getName(),null,sharedPcm);
				tempChild.setParent(tempKing);
				ConversionTypeCl.copyType(serverChild,tempChild );
				sharedChildren.add(tempChild);		
				nonKingFeaturesShared.add(tempChild);

			}

			tempKing.setChildren(sharedChildren);
		}

		sharedPcm.setKingfeatures(kingFeaturesShared);
		sharedPcm.setFeaturesShared(nonKingFeaturesShared);

	}

}
