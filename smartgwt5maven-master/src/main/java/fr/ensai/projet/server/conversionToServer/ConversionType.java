package fr.ensai.projet.server.conversionToServer;

import fr.ensai.projet.server.metier.Feature;
import fr.ensai.projet.server.metier.Type;
import fr.ensai.projet.server.metier.TypeBoolean;
import fr.ensai.projet.server.metier.TypeDate;
import fr.ensai.projet.server.metier.TypeDouble;
import fr.ensai.projet.server.metier.TypeEnum;
import fr.ensai.projet.server.metier.TypeString;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedType;
import fr.ensai.projet.shared.metier.SharedTypeBoolean;
import fr.ensai.projet.shared.metier.SharedTypeDate;
import fr.ensai.projet.shared.metier.SharedTypeDouble;
import fr.ensai.projet.shared.metier.SharedTypeEnum;
import fr.ensai.projet.shared.metier.SharedTypeString;

public class ConversionType {

	/**
	 * Transforme type shared en type server.
	 * @param featureServer
	 * @param featureShared
	 */
	public static void copyType(Feature featureServer,SharedFeature featureShared){
		Type tempServer;
		SharedType tempShared=featureShared.getType();
		if (tempShared instanceof SharedTypeBoolean) {
			tempServer=new TypeBoolean();
			tempServer.setDescription(tempShared.getDescription());
			featureServer.setType(tempServer);
			
			
		} else if (tempShared instanceof SharedTypeString){
			tempServer=new TypeString();
			tempServer.setDescription(tempShared.getDescription());
			featureServer.setType(tempServer);
		}
		else if (tempShared instanceof SharedTypeEnum){
			tempServer=new TypeEnum();
			((TypeEnum)tempServer).setDescription(tempShared.getDescription());
			((TypeEnum)tempServer).setValues((((SharedTypeEnum) tempShared).getValues()));
			featureServer.setType(tempServer);
		}
		else if (tempShared instanceof SharedTypeDate){
			tempServer=new TypeDate();
			tempServer.setDescription(tempShared.getDescription());
			featureServer.setType(tempServer);
		}
		else if (tempShared instanceof SharedTypeDouble){
			tempServer=new TypeDouble();
			tempServer.setDescription(tempShared.getDescription());
			featureServer.setType(tempServer);
		}

	}
}
