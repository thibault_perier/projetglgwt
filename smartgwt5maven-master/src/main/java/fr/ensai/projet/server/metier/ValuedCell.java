package fr.ensai.projet.server.metier;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */

@Entity
@XmlRootElement(name = "ValuedCell")
public class ValuedCell {

	/*
	 * Attributs
	 */
	@Id
	private String id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	private Pcm pcm;

	@ManyToOne
	private Product product;

	@ManyToOne
	private Feature feature;

	private String value = "";

	@OneToOne(cascade = { CascadeType.ALL })
	private Type type = null;

	/**
	 * Accesseurs et mutateurs
	 */
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlIDREF
	public Pcm getPcm() {
		return pcm;
	}

	public void setPcm(Pcm pcm) {
		this.pcm = pcm;
	}

	@XmlIDREF
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	@XmlIDREF
	public Feature getFeature() {
		return feature;
	}

	public void setFeature(Feature feature) {
		this.feature = feature;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@XmlIDREF
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/*
	 * Constructeurs
	 */

	public ValuedCell() {
	}

	/**
	 * Constructeur utilise pour copier une sharedValuedCell
	 * @param product
	 * @param feature
	 * @param pcm
	 * @param value
	 */
	public ValuedCell(Product product, Feature feature, Pcm pcm, String value) {
		this.id = UUID.randomUUID().toString();
		this.feature = feature;
		this.product = product;
		this.pcm = pcm;
		this.value = value;
	}

	/**
	 * Constructeur
	 * 
	 * @param product
	 * @param feature
	 */
	public ValuedCell(Product product, Feature feature, Pcm pcm) {
		this.id = UUID.randomUUID().toString();
		this.feature = feature;
		this.product = product;
		this.pcm = pcm;
		if (feature.getType() != null) {
			this.type = feature.getType();
			this.value = feature.getType().create();
		}
	}
}
