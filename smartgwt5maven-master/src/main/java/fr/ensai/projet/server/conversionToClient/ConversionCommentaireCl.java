package fr.ensai.projet.server.conversionToClient;

import java.util.HashSet;
import java.util.Set;

import fr.ensai.projet.server.metier.Commentaire;
import fr.ensai.projet.shared.metier.SharedCommentaire;

public class ConversionCommentaireCl {

	/**
	 * Methode permettant de convertir des ServerCommentaires en commentaires
	 * (shared)
	 * 
	 * @param serverCommentaires
	 * @return Set<SharedCommentaire>
	 */
	public static Set<SharedCommentaire> convertSetCommentaire(
			Set<Commentaire> serverCommentaires) {
		Set<SharedCommentaire> commentaires = new HashSet<SharedCommentaire>();
		SharedCommentaire convertCommentaire;
		for (Commentaire serverCommentaire : serverCommentaires) {
			convertCommentaire=new SharedCommentaire(serverCommentaire.getContenu(),
					serverCommentaire.getAuteur(), serverCommentaire
					.getDate());
			commentaires
			.add(convertCommentaire);
		}
		return commentaires;
	}
}
