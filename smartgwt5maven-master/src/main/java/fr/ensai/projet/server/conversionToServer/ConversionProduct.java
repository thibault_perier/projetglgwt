package fr.ensai.projet.server.conversionToServer;

import java.util.HashSet;
import java.util.Set;

import fr.ensai.projet.server.metier.Commentaire;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.metier.Product;
import fr.ensai.projet.shared.metier.SharedPcm;
import fr.ensai.projet.shared.metier.SharedProduct;

public class ConversionProduct {

	/**
	 * Methode permettantd de copier les produits d'un objet de classe SharedPcm
	 * dans un objet de classe Pcm
	 * 
	 * @param pcm
	 *            objet de classe Pcm
	 * @param sharedPcm
	 *            objet de classe SharedPcm
	 */
	public static void copyProduct(Pcm pcm, SharedPcm sharedPcm) {
		Set<Commentaire> commentaires = new HashSet<Commentaire>();
		Product product;
		for (SharedProduct sharedProduct : sharedPcm.getProducts()) {
			commentaires = ConversionCommentaire
					.convertSetCommentaire(sharedProduct
							.getCommentairesShared());
			product = new Product(sharedProduct.getId(),
					sharedProduct.getName(), pcm, commentaires);
			pcm.addProduct(product);
		}

	}
}
