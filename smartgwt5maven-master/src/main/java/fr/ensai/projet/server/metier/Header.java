package fr.ensai.projet.server.metier;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlIDREF;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@MappedSuperclass
public abstract class Header {

	/*
	 * Attributs
	 */
	@Id
	protected String id;

	protected String name;

	@ManyToOne(cascade = { CascadeType.ALL })
	protected Pcm pcm;

	/*
	 * Accesseurs et mutateurs + surcharge des operateurs usuels de conteneurs
	 */

	@XmlID
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String newName) {
		this.name = newName;
	}

	@XmlIDREF
	public Pcm getPcm() {
		return this.pcm;
	}

	public void setPcm(Pcm pcm) {
		this.pcm = pcm;
	}

	/*
	 * Constructeurs
	 */

	public Header() {
	}

	public Header(String id, String name, Pcm pcm) {
		this.id = id;
		this.name = name;
		this.pcm = pcm;
	}

}
