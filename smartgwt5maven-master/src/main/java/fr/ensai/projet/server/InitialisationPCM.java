package fr.ensai.projet.server;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBException;

import fr.ensai.projet.server.serialisation.PcmSerializer;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedPcm;
import fr.ensai.projet.shared.metier.SharedProduct;
import fr.ensai.projet.shared.metier.SharedTypeBoolean;
import fr.ensai.projet.shared.metier.SharedTypeDate;
import fr.ensai.projet.shared.metier.SharedTypeDouble;
import fr.ensai.projet.shared.metier.SharedTypeEnum;
import fr.ensai.projet.shared.metier.SharedTypeString;

public class InitialisationPCM {
	public static SharedPcm initPCM() throws IOException, JAXBException {
//		SharedPcm pcm = new SharedPcm("PcmTest", "Mon premier pcm : Comparaison entre pokémons.", "user");
//
//		/**
//		 * initialisation des Features
//		 */
//		Set<SharedFeature> features = new HashSet<SharedFeature>();
//		/**
//		 * initialisation des famille (king)
//		 */
//		SharedFeature caracteristiqueFamille = new SharedFeature("Caractéristique", pcm);
//
//		features.add(caracteristiqueFamille);
//		/**
//		 * initialisation des features simples
//		 */
//		
//		SharedFeature evolutif = new SharedFeature("EstEvolutif",
//				new SharedTypeBoolean(), pcm);
//		
//		SharedFeature description = new SharedFeature("Description",
//				new SharedTypeString(), pcm);
//		
//		HashSet<String> values = new HashSet<String>();
//		values.add("Eclair");
//		values.add("Feu");
//		values.add("Eau");
//		values.add("Paranoïaque");
//		SharedFeature type = new SharedFeature("Type", new SharedTypeEnum(
//				values), pcm);
//		features.add(evolutif);
//		features.add(description);
//		features.add(type);
//		
//		
//		SharedFeature attaque = new SharedFeature("Pt.Attaque",
//				new SharedTypeDouble(), pcm);
//		
//		features.add(attaque);
//		
//		SharedFeature captureDate = new SharedFeature("Date capture",
//				new SharedTypeDate(), pcm);
//		
//		features.add(captureDate);
//		
//		/**
//		 * Attribution des enfants
//		 */
//		caracteristiqueFamille.addChild(type);
//		caracteristiqueFamille.addChild(attaque);
//		caracteristiqueFamille.addChild(description);
//		
//		
//
//		/**
//		 * initialisation des produits simples
//		 */
//		SharedProduct pikachu = new SharedProduct("Pikachu", pcm);
//		SharedProduct salameche = new SharedProduct("Racaillou", pcm);
//		SharedProduct tadmorv = new SharedProduct("Tadmorv", pcm);
//
//		pcm.addProduct(pikachu);
//		pcm.addProduct(salameche);
//		pcm.addProduct(tadmorv);
//		
//		
//		for (SharedFeature feature : features) {
//			pcm.addFeature(feature);
//		}
//		

		String xml="<?xml version='1.0' encoding='UTF-8' standalone='yes'?><pcm><author>user</author><dateCreation>2015-01-29T20:03:52.916+01:00</dateCreation><dateDerniereModif>2015-01-29T20:03:52.916+01:00</dateDerniereModif><description>Mon premier pcm : Comparaison entre pokémons.</description><nonKingFeatures><feature king='false'><id>62</id><name>Pt.Attaque</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><parent>58</parent><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeDouble'><id>e5870c6b-d757-4b88-8958-5c5b037f5610</id><rangeMin>0</rangeMin><rangeMax>0</rangeMax><nbDecimal>0</nbDecimal></type></feature><feature king='false'><id>61</id><name>Type</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><parent>58</parent><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeEnum'><id>b8a8dce1-02e0-4abb-be03-aa45d4054d60</id><values>eclair</values><values>paranoïaque</values><values>feu</values><values>eau</values></type></feature><feature king='false'><id>60</id><name>Description</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><parent>58</parent><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeString'><id>2ce6ef1e-89a1-4238-adac-4316bd5c2c18</id><sizeMax>100</sizeMax></type></feature><feature king='false'><id>59</id><name>EstEvolutif</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeBoolean'><id>dbeb92d3-8da5-4252-8e38-63acc42cd71c</id><item>0</item><item></item><item>1</item><item>t</item><item>f</item><item>true</item><item>false</item></type></feature><feature king='false'><id>63</id><name>Date capture</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeDate'><id>0618cd75-4a67-4de6-b209-c57c7ef02f07</id><pattern><description>DD/MM/YYYY</description></pattern></type></feature></nonKingFeatures><id>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</id><kingfeatures><feature king='true'><id>58</id><name>Caractéristique</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><child king='false'><id>62</id><name>Pt.Attaque</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><parent>58</parent><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeDouble'><id>e5870c6b-d757-4b88-8958-5c5b037f5610</id><rangeMin>0</rangeMin><rangeMax>0</rangeMax><nbDecimal>0</nbDecimal></type></child><child king='false'><id>61</id><name>Type</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><parent>58</parent><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeEnum'><id>b8a8dce1-02e0-4abb-be03-aa45d4054d60</id><values>eclair</values><values>paranoïaque</values><values>feu</values><values>eau</values></type></child><child king='false'><id>60</id><name>Description</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><parent>58</parent><type xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:type='typeString'><id>2ce6ef1e-89a1-4238-adac-4316bd5c2c18</id><sizeMax>100</sizeMax></type></child></feature></kingfeatures><name>PcmTest</name><product><id>66</id><name>Tadmorv</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm></product><product><id>64</id><name>Pikachu</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm></product><product><id>65</id><name>Racaillou</name><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm></product><valuedCell><feature>61</feature><id>7fdd1a72-81e6-45ff-8226-b9e8f5685e4e</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>66</product><type>b8a8dce1-02e0-4abb-be03-aa45d4054d60</type><value></value></valuedCell><valuedCell><feature>62</feature><id>02e8e299-5dc7-4aee-9edc-aadc3f136123</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>64</product><type>e5870c6b-d757-4b88-8958-5c5b037f5610</type><value>4.2</value></valuedCell><valuedCell><feature>61</feature><id>72d2b254-3245-48c6-8791-e66bf3603ed1</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>65</product><type>b8a8dce1-02e0-4abb-be03-aa45d4054d60</type><value>paranoïaque</value></valuedCell><valuedCell><feature>61</feature><id>1daa3088-b755-4f77-b0b0-e329ef485953</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>64</product><type>b8a8dce1-02e0-4abb-be03-aa45d4054d60</type><value></value></valuedCell><valuedCell><feature>59</feature><id>93fd1a5d-e0af-4365-9821-4cf77b4b8622</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>64</product><type>dbeb92d3-8da5-4252-8e38-63acc42cd71c</type><value>T</value></valuedCell><valuedCell><feature>59</feature><id>d620a7b8-292b-47b6-942b-d79e41d50ca7</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>65</product><type>dbeb92d3-8da5-4252-8e38-63acc42cd71c</type><value>T</value></valuedCell><valuedCell><feature>59</feature><id>1ea9c207-7c5a-4311-bde1-eb5e641028fd</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>66</product><type>dbeb92d3-8da5-4252-8e38-63acc42cd71c</type><value>F</value></valuedCell><valuedCell><feature>60</feature><id>940085b8-5ba4-41af-b2a0-36c7ee941afd</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>66</product><type>2ce6ef1e-89a1-4238-adac-4316bd5c2c18</type><value></value></valuedCell><valuedCell><feature>60</feature><id>df7f43fb-4795-40ac-8e86-37a73c70ef2c</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>65</product><type>2ce6ef1e-89a1-4238-adac-4316bd5c2c18</type><value>Caillou avec des bras</value></valuedCell><valuedCell><feature>63</feature><id>8f5133aa-9335-4263-b21a-571eca310fcb</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>65</product><type>0618cd75-4a67-4de6-b209-c57c7ef02f07</type><value></value></valuedCell><valuedCell><feature>63</feature><id>4a869d16-b658-442e-adca-6e371916d2ae</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>66</product><type>0618cd75-4a67-4de6-b209-c57c7ef02f07</type><value></value></valuedCell><valuedCell><feature>62</feature><id>a45854df-8b34-43a0-8f51-0a9d9bcea215</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>66</product><type>e5870c6b-d757-4b88-8958-5c5b037f5610</type><value></value></valuedCell><valuedCell><feature>62</feature><id>c8fa54b9-bee8-4d3b-bac9-ad50118071bc</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>65</product><type>e5870c6b-d757-4b88-8958-5c5b037f5610</type><value></value></valuedCell><valuedCell><feature>63</feature><id>ba6f3f00-4e34-42e1-969a-683a33e3f05d</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>64</product><type>0618cd75-4a67-4de6-b209-c57c7ef02f07</type><value>01/01/2012</value></valuedCell><valuedCell><feature>60</feature><id>44e9f22d-19a0-40a5-b5ae-95112398652f</id><pcm>bdeef6ac-d1b3-48cc-8500-a9c643f794fd</pcm><product>64</product><type>2ce6ef1e-89a1-4238-adac-4316bd5c2c18</type><value>Souris jaune</value></valuedCell></pcm>";
		ControleurMetier ctrl=new ControleurMetier();
		return ctrl.convertFromServer(new PcmSerializer().importPcmString(xml));
	}
}
