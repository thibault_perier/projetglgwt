package fr.ensai.projet.server.serialisation;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.metier.TypeBoolean;
import fr.ensai.projet.server.metier.TypeDate;
import fr.ensai.projet.server.metier.TypeDouble;
import fr.ensai.projet.server.metier.TypeEnum;
import fr.ensai.projet.server.metier.TypeString;

public class PcmSerializer {

	/*
	 * Attribut
	 */
	private final static String FOLDER = PcmSerializer.class
			.getProtectionDomain().getCodeSource().getLocation().getPath()
			+ "pcm_client";

	/*
	 * Accesseur
	 */
	public static String getFOLDER() {
		return FOLDER;
	}

	/*
	 * Constructeur
	 */
	public PcmSerializer() {
	}

	/**
	 * Methode permettant d'exporter un pcm. Cette methode va aussi stocker le
	 * pcm. Le nom du fichier stocke sera le nom du pcm
	 * 
	 * @param pcm
	 *            a exporter.
	 * @throws JAXBException
	 *             cette exception est levee lors d'un probleme avec le
	 *             marshaller
	 * @throws UnsupportedEncodingException
	 *             le fichier xml serialiser sera encode en UTF-8, l'exception
	 *             sera levee en cas d'erreur
	 */
	public String exportPcm(Pcm pcm) throws JAXBException,
			UnsupportedEncodingException {
		File theDirectory = new File(FOLDER);
		try {
			theDirectory.mkdir();
		} catch (SecurityException se) {
			System.err.println(se);
		}

		// ByteArrayOutputStream outStream = new ByteArrayOutputStream();

		String fileNameOnServer = pcm.getName() + "_" + pcm.getAuthor();
		File file = new File(FOLDER + "/" + fileNameOnServer + ".xml");
		JAXBContext jaxbContext;

		jaxbContext = JAXBContext.newInstance(Pcm.class, TypeDouble.class,
				TypeString.class, TypeBoolean.class, TypeDate.class,
				TypeEnum.class);

		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(pcm, file);
		// jaxbMarshaller.marshal(pcm, outStream);
		// return outStream.toString("UTF-8");
		return fileNameOnServer;
	}

	/**
	 * Methode permettant d'instancier un pcm a partir d'un fichier xml
	 * 
	 * @param nomDuPcm
	 *            nom du pcm a importer, il n'y a pas besoin
	 * @return
	 * @throws IOException
	 *             si le nom du fichier n'existe pas
	 * @throws JAXBException
	 *             si il y a un problem jaxb
	 * 
	 */
	public Pcm importPcm(String nomDuPcm) throws IOException, JAXBException {
		String fullName;
		if (nomDuPcm.endsWith(".xml")) {
			fullName = nomDuPcm;
		} else {
			fullName = nomDuPcm + ".xml";
		}
		File file = new File(FOLDER + "/" + fullName);
		JAXBContext jaxbContext = JAXBContext.newInstance(Pcm.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Pcm importPcm = (Pcm) jaxbUnmarshaller.unmarshal(file);
		return importPcm;
	}

	

	/**
	 * Methode permettant d'instancier un pcm a partir d'un fichier xml au format string
	 * 
	 * @param xml
	 *            string xml
	 * @return
	 * @throws IOException
	 *             si le nom du fichier n'existe pas
	 * @throws JAXBException
	 *             si il y a un problem jaxb
	 * 
	 */
	public Pcm importPcmString(String xml) throws IOException, JAXBException {
			
		StringReader reader = new StringReader(xml);
		JAXBContext jaxbContext = JAXBContext.newInstance(Pcm.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Pcm importPcm = (Pcm) jaxbUnmarshaller.unmarshal(reader);
		return importPcm;
	}
	
	/**
	 * Permet de stocker un pcm sur le serveur en version .xml
	 * @param pcm
	 * @return
	 * @throws JAXBException
	 */
	public String sauvegarder(Pcm pcm) throws JAXBException {
		File theDirectory = new File(FOLDER);
		try {
			theDirectory.mkdir();
		} catch (SecurityException se) {
			System.err.println(se);
		}
		String fileNameOnServer = pcm.getName() + "[" + pcm.getAuthor() + "]";
		File file = new File(FOLDER + "/" + fileNameOnServer + ".xml");
		JAXBContext jaxbContext;
		jaxbContext = JAXBContext.newInstance(Pcm.class, TypeDouble.class,
				TypeString.class, TypeBoolean.class, TypeDate.class,
				TypeEnum.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(pcm, file);
		return "Pcm sauvegarde sous le nom : " + fileNameOnServer;
	}

}
