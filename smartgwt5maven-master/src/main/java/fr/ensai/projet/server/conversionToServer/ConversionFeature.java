package fr.ensai.projet.server.conversionToServer;

import java.util.HashSet;
import java.util.Set;

import fr.ensai.projet.server.metier.Feature;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedPcm;

public class ConversionFeature {

	/**
	 * Transforme features Shared en features Server.
	 * @param pcm
	 * @param sharedPcm
	 */
	public static void copyFeature(Pcm pcm,SharedPcm sharedPcm){

		//Features non king with or without a king parent
		Set<SharedFeature> nonKingFeaturesShared=sharedPcm.getFeaturesShared();
		Set<Feature> nonKingFeaturesServer= new HashSet<Feature>();

		//Features non king and without a king parent
		Set<SharedFeature> noParentFeaturesShared=new HashSet<SharedFeature>();

		for (SharedFeature featureShared : nonKingFeaturesShared) {
			if (featureShared.getParent()==null) {
				noParentFeaturesShared.add(featureShared);
			}
		}

		//Copie des features sans parents
		Feature tempCopie;
		for (SharedFeature featureShared : noParentFeaturesShared) {
			tempCopie=new Feature(featureShared.getId(), featureShared.getName(), pcm, null);
			tempCopie.setParent(null);
			ConversionType.copyType(tempCopie, featureShared);
			nonKingFeaturesServer.add(tempCopie);

		}

		//Récupère tout les Features king
		Set<SharedFeature> kingFeaturesShared=sharedPcm.getKingfeatures();
		Set<Feature> kingFeaturesServer= new HashSet<Feature>();

		Feature tempKing;
		Feature tempChild;
		for (SharedFeature sharedHeader : kingFeaturesShared) {

			//Copie du parents sans enfants.
			tempKing=new Feature(sharedHeader.getId(), sharedHeader.getName(), pcm);
			kingFeaturesServer.add(tempKing);
			//Ajout à ce parents des enfants de shared			
			Set<SharedFeature> sharedChildren=sharedHeader.getChildren();
			Set<Feature> childrenServer=new HashSet<Feature>();

			for (SharedFeature sharedChild : sharedChildren) {
				//Ajouter la copie du type pour retier l'erreur.
				tempChild=new Feature(sharedChild.getId(), sharedChild.getName(), pcm, null);
				tempChild.setParent(tempKing);
				ConversionType.copyType(tempChild, sharedChild);
				childrenServer.add(tempChild);		
				nonKingFeaturesServer.add(tempChild);

			}

			tempKing.setChildren(childrenServer);
		}

		pcm.setKingfeatures(kingFeaturesServer);
		pcm.setFeatures(nonKingFeaturesServer);

	}

}
