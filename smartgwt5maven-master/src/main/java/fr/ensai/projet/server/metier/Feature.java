package fr.ensai.projet.server.metier;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "Feature")
public class Feature extends Header {

	/*
	 * Attributs
	 */
	@OneToOne(cascade = { CascadeType.ALL })
	private Type type;

	private boolean isKing;

	@OneToMany(targetEntity = Feature.class, cascade = { CascadeType.ALL })
	private Set<Feature> children = new HashSet<Feature>();

	@ManyToOne(targetEntity = Feature.class, cascade = { CascadeType.ALL })
	private Feature parent;

	/*
	 * Accesseurs et mutateurs + surcharge des operateurs usuels de conteneurs
	 */
	@XmlElement(name = "child")
	public Set<Feature> getChildren() {
		return this.children;
	}

	public int getNbChildren() {
		return this.children.size();
	}

	public void setChildren(Set<Feature> children) {
		this.children.addAll(children);
	}

	@XmlIDREF
	public Feature getParent() {
		return this.parent;
	}

	public void setParent(Feature parent) {
		this.parent = parent;
	}

	@XmlAttribute
	public boolean isKing() {
		return isKing;
	}

	public void setKing(boolean isKing) {
		this.isKing = isKing;
	}

	@XmlElement(name = "type")
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	/*
	 * Constructeurs
	 */

	/**
	 * Constructeur vide pour la (de)serialisation
	 */
	public Feature() {
		super();
	}

	/**
	 * Constructeur pour une feature qui n'est pas une famille et qui n'en a pas
	 * non plus (ie qui a un type)
	 * 
	 * @param name
	 * @param pcm
	 * @param isKing
	 * @param type
	 */
	public Feature(String id, String name, Pcm pcm, Type type) {
		super(id, name, pcm);
		this.type = type;
		this.isKing = false;
	}

	/**
	 * Constructeur pour une feature qui n'est pas une famille mais qui a une
	 * famille (et qui a un type)
	 * 
	 * @param id
	 * @param name
	 * @param pcm
	 * @param type
	 * @param parent
	 */
	public Feature(String id, String name, Pcm pcm, Type type, Feature parent) {
		super(id, name, pcm);
		this.type = type;
		this.isKing = false;
		this.parent = parent;
	}

	/**
	 * Constructeur pour une famille de feature (pas de type)
	 * 
	 * @param name
	 * @param pcm
	 * @param isKing
	 */
	public Feature(String id, String name, Pcm pcm) {
		super(id, name, pcm);
		this.isKing = true;
	}

}
