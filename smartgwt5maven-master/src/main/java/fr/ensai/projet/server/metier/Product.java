package fr.ensai.projet.server.metier;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Product extends Header {

	/*
	 * Attributs
	 */
	@OneToMany(mappedBy = "product", cascade = { CascadeType.ALL })
	private Set<Commentaire> commentaires = new HashSet<Commentaire>();

	/*
	 * Accesseurs et mutateurs
	 */
	@XmlElement(name = "commentaire")
	public Set<Commentaire> getCommentaires() {
		return commentaires;
	}

	public boolean addCommentaire(Commentaire commentaire) {
		commentaire.setProduct(this);
		return commentaires.add(commentaire);
	}

	/*
	 * Constructeurs
	 */

	public Product() {
		super();
	}

	/**
	 * Constructeur minimum pour la serialisation (utilisé en test)
	 * 
	 * @param id
	 * @param name
	 * @param pcm
	 */
	public Product(String id, String name, Pcm pcm) {
		super(id, name, pcm);
	}

	/**
	 * Constructeur de test pour un produit
	 * 
	 * @param name
	 *            nom du produit
	 * @param pcm
	 *            auquel appartient le produit
	 * @param parent
	 *            famille de produits éventuellement null
	 */
	public Product(String id, String name, Pcm pcm,
			Set<Commentaire> commentaires) {
		super(id, name, pcm);
		for (Commentaire commentaire : commentaires) {
			addCommentaire(commentaire);
		}
	}

}
