package fr.ensai.projet.server.metier;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlIDREF;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "Commentaire")
public class Commentaire {

	/**
	 * Attributs
	 */
	@Id
	private String id;

	private String texte;

	private String auteur;

	@Temporal(TemporalType.DATE)
	private Date date;

	@ManyToOne(cascade = { CascadeType.ALL })
	private Product product;

	/**
	 * Accesseurs et mutateurs
	 */

	@XmlElement
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@XmlElement
	public String getContenu() {
		return texte;
	}

	public void setContenu(String contenu) {
		this.texte = contenu;
	}

	@XmlElement
	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	@XmlElement
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@XmlIDREF
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	/*
	 * Constructeurs
	 */

	/**
	 * Constructeur vide pour (de)serialiser
	 */
	public Commentaire() {
		super();
	}

	/**
	 * Constructeur pour copier les commentaires
	 * 
	 * @param texte
	 * @param auteur
	 * @param date
	 */
	public Commentaire(String texte, String auteur, Date date) {
		super();
		this.id = UUID.randomUUID().toString();
		this.texte = texte;
		this.auteur = auteur;
		this.date = date;
	}
	
	
}
