package fr.ensai.projet.server.metier;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Pcm {

	/*
	 * Attributs indispensables a la construction du pcm
	 */
	@Id
	private String id;

	private String name;

	private String description;

	private String author;

	@Temporal(TemporalType.DATE)
	private Date dateCreation;

	@Temporal(TemporalType.DATE)
	private Date dateDerniereModif;

	/*
	 * Composants du pcm
	 */

	@OneToMany(mappedBy = "Pcm", targetEntity = Product.class, cascade = { CascadeType.ALL })
	private Set<Product> products = new HashSet<Product>();

	@OneToMany(mappedBy = "Pcm", targetEntity = Feature.class, cascade = { CascadeType.ALL })
	private Set<Feature> kingfeatures;

	@OneToMany(mappedBy = "Pcm", targetEntity = Feature.class, cascade = { CascadeType.ALL })
	private Set<Feature> features = new HashSet<Feature>();

	@OneToMany(mappedBy = "pcm", cascade = { CascadeType.ALL })
	private Set<ValuedCell> valuedCells = new HashSet<ValuedCell>();

	/*
	 * Accesseurs et mutateurs
	 */

	@XmlID
	public String getId() {
		return id;
	}

	@XmlElementWrapper(name = "kingfeatures")
	@XmlElement(name = "feature")
	public Set<Feature> getKingfeatures() {
		return kingfeatures;
	}

	public void setKingfeatures(Set<Feature> kingfeatures) {
		this.kingfeatures = kingfeatures;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDateDerniereModif() {
		return dateDerniereModif;
	}

	public void setDateDerniereModif(Date dateDerniereModif) {
		this.dateDerniereModif = dateDerniereModif;
	}

	public Date getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}

	@XmlElement(name = "product")
	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	@XmlElementWrapper(name = "nonKingFeatures")
	@XmlElement(name = "feature")
	public Set<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(Set<Feature> features) {
		this.features = features;
	}

	@XmlElement(name = "valuedCell")
	public Set<ValuedCell> getValuedCells() {
		return valuedCells;
	}

	public void setValuedCells(Set<ValuedCell> valuedCells) {
		this.valuedCells = valuedCells;
	}

	/*
	 * Constructeurs
	 */

	/**
	 * Constructeur vide pour la (de)serialisation
	 */
	public Pcm() {
		this.dateCreation = new Date();
		this.dateDerniereModif = this.dateCreation;
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 * @param description
	 * @param author
	 */
	public Pcm(String name, String description, String author) {
		this.id = UUID.randomUUID().toString();
		this.name = name;
		this.description = description;
		this.author = author;
		this.dateCreation = new Date();
		this.dateDerniereModif = this.dateCreation;
	}

	/*
	 * Methodes fonctionnelles
	 */

	/**
	 * Methode permettant d'ajouter un produit ou une famille de produit au pcm.
	 * 
	 * @param product
	 * @return True si le produit a ete correctement ajoute
	 */
	public boolean addProduct(Product product) {


		if (this.products.add(product)) {
			this.dateDerniereModif = new Date();
			product.setPcm(this);
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Methode pour ajouter une feature ou une famille de feature au PCM.
	 * 
	 * @param feature
	 *            ajouter
	 * @return true si la feature a ete ajoutee avec succes
	 */
	public boolean addFeature(Feature feature) {

		if (this.features.add(feature)) {
			this.dateDerniereModif = new Date();
			feature.setPcm(this);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	* Methode permettant d'ajouter une valuedCell au pcm.
	* 
	* @param valuedCell
	*/
	public void addValuedCell(ValuedCell valuedCell) {
		valuedCell.setPcm(this);
		this.valuedCells.add(valuedCell);
	}

	/**
	 * Methode permettant de trouver un produit grace a son id
	 * 
	 * @param id
	 *            du product recherche
	 * @return product si trouve dans le pcm, null sinon
	 */
	public Product findProductById(String id) {
		Product productWithSameId = null;
		for (Product product : products) {
			if (product.getId().equals(id)) {
				productWithSameId = product;
			}
		}
		return productWithSameId;
	}

	/**
	 * Methode permettantd de trouver une feature grace a son id
	 * 
	 * @param id
	 *            de la feature recherchee
	 * @return feature trouvee ou null si la feature n'a pas ete trouvee
	 */
	public Feature findFeatureById(String id) {
		Feature featureWithSameId = null;
		for (Feature feature : features) {
			if (feature.getId().equals(id)) {
				featureWithSameId = feature;
			}
		}
		return featureWithSameId;
	}
}
