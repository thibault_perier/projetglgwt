package fr.ensai.projet.server.conversionToServer;

import java.util.HashSet;
import java.util.Set;

import fr.ensai.projet.server.metier.Commentaire;
import fr.ensai.projet.shared.metier.SharedCommentaire;

public class ConversionCommentaire {

	/**
	 * Methode permettant de convertir des SharedCommentaires en commentaires
	 * (metier)
	 * 
	 * @param sharedCommentaires
	 * @return Set<Commentaire>
	 */
	public static Set<Commentaire> convertSetCommentaire(
			Set<SharedCommentaire> sharedCommentaires) {
		Set<Commentaire> commentaires = new HashSet<Commentaire>();
		Commentaire convertCommentaire;
		for (SharedCommentaire sharedCommentaire : sharedCommentaires) {
			 convertCommentaire=new Commentaire(sharedCommentaire.getTexte(),
					sharedCommentaire.getAuteur(), sharedCommentaire
							.getDate());
			commentaires
					.add(convertCommentaire);
		}
		return commentaires;
	}
}
