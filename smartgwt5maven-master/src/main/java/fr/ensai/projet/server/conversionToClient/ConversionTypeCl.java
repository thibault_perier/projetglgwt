package fr.ensai.projet.server.conversionToClient;

import fr.ensai.projet.server.metier.Feature;
import fr.ensai.projet.server.metier.Type;
import fr.ensai.projet.server.metier.TypeBoolean;
import fr.ensai.projet.server.metier.TypeDate;
import fr.ensai.projet.server.metier.TypeDouble;
import fr.ensai.projet.server.metier.TypeEnum;
import fr.ensai.projet.server.metier.TypeString;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedType;
import fr.ensai.projet.shared.metier.SharedTypeBoolean;
import fr.ensai.projet.shared.metier.SharedTypeDate;
import fr.ensai.projet.shared.metier.SharedTypeDouble;
import fr.ensai.projet.shared.metier.SharedTypeEnum;
import fr.ensai.projet.shared.metier.SharedTypeString;

public class ConversionTypeCl {

	/**
	 * Transforme un Type serveur en SharedType.
	 * @param featureServer
	 * @param featureShared
	 */
	public static void copyType(Feature featureServer,SharedFeature featureShared){
		Type tempServer=featureServer.getType();
		SharedType tempShared;
		if (tempServer instanceof TypeBoolean) {
			tempShared=new SharedTypeBoolean();
			tempShared.setDescription(tempServer.getDescription());
			featureShared.setTypeShared(tempShared);


		} else if (tempServer instanceof TypeString){
			tempShared=new SharedTypeString();
			tempShared.setDescription(tempShared.getDescription());
			featureShared.setTypeShared(tempShared);
		}
		else if (tempServer instanceof TypeEnum){
			tempShared=new SharedTypeEnum();
			((SharedTypeEnum)tempShared).setDescription(tempServer.getDescription());
			((SharedTypeEnum)tempShared).setValues((((TypeEnum) tempServer).getValues()));
			featureShared.setTypeShared(tempShared);
		}
		else if (tempServer instanceof TypeDate){
			tempShared=new SharedTypeDate();
			tempShared.setDescription(tempShared.getDescription());
			featureShared.setTypeShared(tempShared);
		}
		else if (tempServer instanceof TypeDouble){
			tempShared=new SharedTypeDouble();
			tempShared.setDescription(tempShared.getDescription());
			featureShared.setTypeShared(tempShared);
		}

	}
}
