package fr.ensai.projet.server.serviceImpl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import fr.ensai.projet.server.serialisation.PcmSerializer;

/**
 * 02 * servlet to handle file upload requests 03 * 04 * @author hturksoy 05 *
 * 06
 */
public class FileUploadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String PATH = PcmSerializer.getFOLDER();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// process only multipart requests
		if (ServletFileUpload.isMultipartContent(req)) {

			// Create a factory for disk-based file items
			FileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Parse the request
			try {
				@SuppressWarnings("unchecked")
				List<FileItem> items = upload.parseRequest(req);
				for (FileItem item : items) {
					// process only file upload - discard other form item types
					if (item.isFormField())
						continue;

					String fileName = item.getName();
					// get only the file name not whole path
					if (fileName != null) {
						fileName = FilenameUtils.getName(fileName);
					}

					File uploadedFile = new File(PATH, fileName);

					File theDirectory = new File(PATH);
					try {
						theDirectory.mkdir();
					} catch (SecurityException se) {
						System.err.println(se);
					}

					if (uploadedFile.createNewFile()) {
						item.write(uploadedFile);
						resp.setContentType("text/html");
						String temp = "Upload succeeded";
						resp.getWriter().printf("{ \"Resultat\": \"%s\" }",
								temp);
						// resp.getWriter().print("sucess");
						resp.flushBuffer();
					} else
						item.write(uploadedFile);
					resp.setContentType("text/html");
					String temp = "Upload succeeded";
					resp.getWriter().printf("{ \"Resultat\": \"%s\" }",
							temp);
					// resp.getWriter().print("sucess");
					resp.flushBuffer();
				}
			} catch (Exception e) {
				resp.sendError(
						HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
						"An error occurred while creating the file : "
								+ e.getMessage());
			}

		} else {
			resp.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE,
					"Request contents type is not supported by the servlet.");
		}
	}

}