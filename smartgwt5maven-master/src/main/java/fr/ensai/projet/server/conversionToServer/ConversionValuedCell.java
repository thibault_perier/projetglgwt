package fr.ensai.projet.server.conversionToServer;

import fr.ensai.projet.server.metier.Feature;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.metier.Product;
import fr.ensai.projet.server.metier.ValuedCell;
import fr.ensai.projet.shared.metier.SharedPcm;
import fr.ensai.projet.shared.metier.SharedValuedCell;

public class ConversionValuedCell {

	/**
	 * Methode permettant de copier les sharedValuedCell d'un sharedPcm dans un
	 * Pcm
	 * 
	 * @param pcm
	 *            objet de classe Pcm
	 * @param sharedPcm
	 *            objet de classe SharedPcm
	 */
	public static void copyValuedCell(Pcm pcm, SharedPcm sharedPcm) {
		Product product;
		Feature feature;
		for (SharedValuedCell sharedValuedCell : sharedPcm
				.getValuedCellsShared()) {
			product = pcm
					.findProductById(sharedValuedCell.getProduct().getId());
			feature = pcm
					.findFeatureById(sharedValuedCell.getFeature().getId());
			if (product != null && feature != null) {
				ValuedCell temp=new ValuedCell(product, feature,pcm);
				temp.setValue(sharedValuedCell.getValue());
				pcm.addValuedCell(temp);
			}
		}

	}

}
