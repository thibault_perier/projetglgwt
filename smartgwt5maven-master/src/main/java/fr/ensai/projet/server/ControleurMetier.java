package fr.ensai.projet.server;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import fr.ensai.projet.server.conversionToClient.ConversionFeatureCl;
import fr.ensai.projet.server.conversionToClient.ConversionProductCl;
import fr.ensai.projet.server.conversionToClient.ConversionValuedCellCl;
import fr.ensai.projet.server.conversionToServer.ConversionFeature;
import fr.ensai.projet.server.conversionToServer.ConversionProduct;
import fr.ensai.projet.server.conversionToServer.ConversionValuedCell;
import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.serialisation.PcmSerializer;
import fr.ensai.projet.shared.metier.SharedPcm;

public class ControleurMetier {
	/*
	 * Attribut
	 */
	private Pcm pcm;
	private PcmSerializer pcmSerializer = new PcmSerializer();

	/*
	 * Accesseurs et mutateurs
	 */
	public Pcm getPcm() {
		return pcm;
	}

	public void setPcm(Pcm pcm) {
		this.pcm = pcm;
	}

	public PcmSerializer getPcmSerializer() {
		return pcmSerializer;
	}

	public void setPcmSerializer(PcmSerializer pcmSerializer) {
		this.pcmSerializer = pcmSerializer;
	}

	/*
	 * Constructeur
	 */
	public ControleurMetier() {
		super();
	}

	/*
	 * Methodes fonctionnelles
	 */

	/**
	 * Cette methode permet de de convertir un pcmShared en un pcm (metier)
	 * 
	 * @param sharedPcm
	 *            a convertir
	 * @return objet de classe pcm
	 */
	public Pcm convertFromShared(SharedPcm sharedPcm) {
		Pcm pcm = new Pcm(sharedPcm.getName(), sharedPcm.getDescription(),
				sharedPcm.getAuthor());
		ConversionFeature.copyFeature(pcm, sharedPcm);
		ConversionProduct.copyProduct(pcm, sharedPcm);
		ConversionValuedCell.copyValuedCell(pcm, sharedPcm);
		return pcm;
	}
	
	/**
	 * Cette methode permet de de convertir un pcm en un pcmShared (client)/
	 * @param pcm
	 * @return
	 */
	public SharedPcm convertFromServer(Pcm pcm) {

		SharedPcm sharedPcm = new SharedPcm(pcm.getName(),
				pcm.getDescription(), pcm.getAuthor());
		ConversionFeatureCl.copyFeature(pcm, sharedPcm);
		ConversionProductCl.copyProduct(pcm, sharedPcm);
		ConversionValuedCellCl.copyValuedCell(pcm, sharedPcm);

		return sharedPcm;
	}

	/**
	 * Permet d'importer un PCM à partir d'un nom de fichier (xml)
	 * @param name
	 * @return
	 * @throws IOException
	 * @throws JAXBException
	 */
	public Pcm importUploadedPcm(String name) throws IOException, JAXBException {
		return pcmSerializer.importPcm(name);
	}

	/**
	 * Serialize un Pcm et stockage sur le serveur.
	 * @param pcm
	 * @return
	 * @throws JAXBException
	 */
	public String sauvegarder(Pcm pcm) throws JAXBException {
		return pcmSerializer.sauvegarder(pcm);
	}
}
