package fr.ensai.projet.server.conversionToClient;

import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.metier.ValuedCell;
import fr.ensai.projet.shared.metier.SharedFeature;
import fr.ensai.projet.shared.metier.SharedPcm;
import fr.ensai.projet.shared.metier.SharedProduct;
import fr.ensai.projet.shared.metier.SharedValuedCell;

public class ConversionValuedCellCl {

	/**
	 * Methode permettant de copier les sharedValuedCell d'un sharedPcm dans un
	 * Pcm
	 * 
	 * @param pcm
	 *            objet de classe Pcm
	 * @param sharedPcm
	 *            objet de classe SharedPcm
	 */
	public static void copyValuedCell(Pcm pcm, SharedPcm sharedPcm) {
		SharedProduct product;
		SharedFeature feature;
		for (ValuedCell serverValuedCell : pcm.getValuedCells()) {
			product = sharedPcm
					.findProductById(serverValuedCell.getProduct().getId());
			feature = sharedPcm
					.findFeatureById(serverValuedCell.getFeature().getId());
			if (product != null && feature != null) {
				SharedValuedCell temp=new SharedValuedCell(product, feature);
				temp.setValue(serverValuedCell.getValue());
				sharedPcm.addValuedCell(temp);
			}
		}

	}
	
	

}
