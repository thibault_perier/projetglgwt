package fr.ensai.projet.server.conversionToClient;

import java.util.HashSet;
import java.util.Set;

import fr.ensai.projet.server.metier.Pcm;
import fr.ensai.projet.server.metier.Product;
import fr.ensai.projet.shared.metier.SharedCommentaire;
import fr.ensai.projet.shared.metier.SharedPcm;
import fr.ensai.projet.shared.metier.SharedProduct;

public class ConversionProductCl {

	/**
	 * Methode permettantd de copier les produits d'un objet de classe SharedPcm
	 * dans un objet de classe Pcm
	 * 
	 * @param pcm
	 *            objet de classe Pcm
	 * @param sharedPcm
	 *            objet de classe SharedPcm
	 */
	public static void copyProduct(Pcm pcm, SharedPcm sharedPcm) {
		Set<SharedCommentaire> commentaires = new HashSet<SharedCommentaire>();
		SharedProduct product;
		for (Product serverProduct : pcm.getProducts()) {
			commentaires = ConversionCommentaireCl
					.convertSetCommentaire(serverProduct
							.getCommentaires());
			
			product = new SharedProduct(serverProduct.getId(),serverProduct.getName(), sharedPcm, commentaires);
			
			sharedPcm.addProductCopy(product);
		}

	}
}
